import time
from scrapy.crawler import CrawlerProcess
from price.price.spiders.product_list import ProductListSpider
from price.price.spiders.product_list_selenium import ProductListSpiderSelenium
from price.price.spiders.brand_list import BrandListSpider
from scrapy.utils.project import get_project_settings
from postgresql import *
from crochet import setup
import logging
import threading
from datetime import datetime, timedelta
import _constants
import os
import uuid
import sys
import shutil

setup()
# sys.path.append(os.path.join(os.path.curdir, "price/price"))
# os.environ['SCRAPY_SETTINGS_MODULE'] = 'price.price.settings'
logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.DEBUG)

def run_crawler(url, job_id):
    s = get_project_settings()
    s['SPIDER_MODULES'] = ['price.price.spiders']
    s['NEWSPIDER_MODULE'] = 'price.price.spiders'
    s['CONCURRENT_REQUESTS'] = 100
    s['REACTOR_THREADPOOL_MAXSIZE'] = 20
    # s['ROBOTSTXT_OBEY'] = True
    s['AUTOTHROTTLE_ENABLED'] = True
    s['CRAWLERA_PRESERVE_DELAY'] = True
    s['BOT_NAME'] = 'price_bot_selenium'
    s['SPIDER_MIDDLEWARES'] = {
        'price.price.middlewares.PriceSpiderMiddleware': 543,
    }
    s['HTTPCACHE_ENABLED'] = False
    s['DOWNLOADER_MIDDLEWARES'] = {
        # 'price.price.middlewares.PriceDownloaderMiddleware': 543,
        'scrapy_crawlera.CrawleraMiddleware': 610,
        'price.price.middlewares.SeleniumMiddleware': 200
    }
    s['DUPEFILTER_DEBUG'] = True
    s['ITEM_PIPELINES'] = {
        'price.price.pipelines.PricePipeline': 300,
    }
    print('project_settings keys: %s' % [key for key in s.keys()])
    print('DOWNLOADER_MIDDLEWARES: %s' % [i for i in s.get('DOWNLOADER_MIDDLEWARES')])
    process = CrawlerProcess(settings=s)
    if os.path.isdir("crawls"):
        shutil.rmtree("crawls")
    process.crawl(ProductListSpiderSelenium, index_url=url, job_id=job_id)


def daruma_project_settings(jobdir, spider):
    s = get_project_settings()
    s['CONCURRENT_REQUESTS'] = 1
    # s['SPIDER_MODULES'] = ['price.price.spiders']
    # s['NEWSPIDER_MODULE'] = 'price.price.spiders'
    # s['ROBOTSTXT_OBEY'] = True
    # s['REACTOR_THREADPOOL_MAXSIZE'] = 20
    s['AUTOTHROTTLE_ENABLED'] = True
    # s['AUTOTHROTTLE_START_DELAY'] = 0.1
    # s['AUTOTHROTTLE_MAX_DELAY'] = 0.5
    s['BOT_NAME'] = 'price_bot_selenium'
    s['SPIDER_MIDDLEWARES'] = {
        'price.price.middlewares.PriceSpiderMiddleware': 543,
    }
    s['HTTPCACHE_ENABLED'] = False
    s['DOWNLOADER_MIDDLEWARES'] = {
        # 'price.price.middlewares.PriceDownloaderMiddleware': 543,
        'scrapy_crawlera.CrawleraMiddleware': 610,
        'price.price.middlewares.SeleniumMiddleware': 200
    }
    s['JOBDIR'] = jobdir % spider
    # s['DEPTH_PRIORITY'] = 1
    # s['SCHEDULER_DISK_QUEUE'] = 'scrapy.squeues.PickleFifoDiskQueue'
    # s['SCHEDULER_MEMORY_QUEUE'] = 'scrapy.squeues.FifoMemoryQueue'
    s['ITEM_PIPELINES'] = {
        'price.price.pipelines.PricePipeline': 300,
    }
    return s

def run_spider():
    if _constants.PRODUCTION:
        print('crawler running in PRODUCTION mode')
    else:
        print('crawler running in DEVELOPMENT mode')

    my_pid = str(uuid.uuid4())[:5]
    jobdir = 'crawls/%s'

    while True:
        if is_priority_true():
            jobs = get_priority_pending_jobs()
            for job in jobs:
                job_id = job.get("job_id", None)
                url = job.get("url", None)
                spider = job['spider']
                if spider == 'BrandListSpider':
                    s = get_project_settings()
                    s['CONCURRENT_REQUESTS'] = 100
                    s['AUTOTHROTTLE_ENABLED'] = True
                    # s['AUTOTHROTTLE_START_DELAY'] = 0.1
                    # s['AUTOTHROTTLE_MAX_DELAY'] = 0.5
                    # s['REACTOR_THREADPOOL_MAXSIZE'] = 20
                    s['JOBDIR'] = jobdir % spider
                    # s['DEPTH_PRIORITY'] = 1
                    # s['SCHEDULER_DISK_QUEUE'] = 'scrapy.squeues.PickleFifoDiskQueue'
                    # s['SCHEDULER_MEMORY_QUEUE'] = 'scrapy.squeues.FifoMemoryQueue'
                    process = CrawlerProcess(settings=s)
                    update_status_job(job_id=job_id, status='on process')
                    # update_status_job_with_process_id(job_id=job_id, status='on process', process_id=my_pid)
                    process.crawl(BrandListSpider, index_url=url, job_id=job_id, jobdir=jobdir % spider)
                elif spider == 'ProductListSpider':
                    s = get_project_settings()
                    s['AUTOTHROTTLE_ENABLED'] = True
                    # s['AUTOTHROTTLE_START_DELAY'] = 0.1
                    # s['AUTOTHROTTLE_MAX_DELAY'] = 0.5
                    s['CONCURRENT_REQUESTS'] = 100
                    # s['REACTOR_THREADPOOL_MAXSIZE'] = 20
                    s['JOBDIR'] = jobdir % spider
                    # s['DEPTH_PRIORITY'] = 1
                    # s['SCHEDULER_DISK_QUEUE'] = 'scrapy.squeues.PickleFifoDiskQueue'
                    # s['SCHEDULER_MEMORY_QUEUE'] = 'scrapy.squeues.FifoMemoryQueue'
                    process = CrawlerProcess(settings=s)
                    process.crawl(ProductListSpider, index_url=url, job_id=job_id, jobdir=jobdir % spider)
                    update_status_job(job_id=job_id, status='on process')
                    # update_status_job_with_process_id(job_id=job_id, status='on process', process_id=my_pid)
        else:
            logging.warning("no pending priority jobs in database")
            if not is_status_on_process_by_process_id(process_id=my_pid):
                scheduled_job = get_scheduled_pending_jobs()
                if len(scheduled_job) > 0:
                    job = scheduled_job[0]
                    datetime_job = job['schedule_time']
                    datetime_now = datetime.now()
                    duration = datetime_job - datetime_now
                    duration_in_minutes = int(divmod(duration.total_seconds(), 60)[0]) + 1
                    logging.warning('DURATION: %s minutes' % duration_in_minutes)
                    datetime_job_str = job['schedule_time'].strftime("%Y-%m-%d %H:%M")
                    datetime_now_str = datetime.now().strftime("%Y-%m-%d %H:%M")
                    logging.warning('DATETIME JOB: %s' % datetime_job_str)
                    logging.warning('DATETIME NOW: %s' % datetime_now_str)
                    logging.warning('job_id: %s' % job['job_id'])
                    if duration_in_minutes <= 0:
                        job_id = job.get("job_id", None)
                        url = job.get("url", None)
                        spider = job.get("spider", None)
                        created_by = job.get('created_by', None)
                        daruma_code = job.get('daruma_code', None)
                        repeat_days = get_repeat_days(job_id)
                        repeat_days = repeat_days if repeat_days is not None else 0
                        priority = job['priority'] if job['priority'] is not None else False
                        if repeat_days > 0:
                            next_schedule_time = datetime_job + timedelta(days=int(repeat_days))
                            auto_insert_job(url=url,
                                            created_by=created_by,
                                            daruma_code=daruma_code,
                                            spider=spider,
                                            schedule_time=next_schedule_time,
                                            repeat_days=repeat_days,
                                            priority=priority)
                        if spider == 'BrandListSpider':
                            s = get_project_settings()
                            s['CONCURRENT_REQUESTS'] = 100
                            # s['REACTOR_THREADPOOL_MAXSIZE'] = 20
                            s['JOBDIR'] = jobdir % spider
                            s['AUTOTHROTTLE_ENABLED'] = True
                            s['AUTOTHROTTLE_START_DELAY'] = 0.1
                            s['AUTOTHROTTLE_MAX_DELAY'] = 0.5
                            # s['DEPTH_PRIORITY'] = 1
                            # s['SCHEDULER_DISK_QUEUE'] = 'scrapy.squeues.PickleFifoDiskQueue'
                            # s['SCHEDULER_MEMORY_QUEUE'] = 'scrapy.squeues.FifoMemoryQueue'

                            process = CrawlerProcess(settings=s)

                            update_status_job_with_process_id(job_id=job_id, status='on process', process_id=my_pid)
                            process.crawl(BrandListSpider, index_url=url, job_id=job_id, jobdir=jobdir % spider)
                        elif spider == 'ProductListSpider':
                            if 'tokopedia.com' in url:
                                s = daruma_project_settings(jobdir=jobdir, spider=spider)
                                print('project_settings keys: %s' % [key for key in s.keys()])
                                print('DOWNLOADER_MIDDLEWARES: %s' % [i for i in s.get('DOWNLOADER_MIDDLEWARES')])
                                process = CrawlerProcess(settings=s)
                                if os.path.isdir("crawls"):
                                    shutil.rmtree("crawls")
                                    print("directory crawls has been removed")
                                process.crawl(ProductListSpiderSelenium, index_url=url, job_id=job_id,
                                              jobdir=jobdir % spider)
                                # update_status_job(job_id=job_id, status='on process')
                                update_status_job_with_process_id(job_id=job_id, status='on process', process_id=my_pid)
                            else:
                                s = get_project_settings()
                                s['JOBDIR'] = jobdir % spider
                                s['AUTOTHROTTLE_ENABLED'] = True
                                s['AUTOTHROTTLE_START_DELAY'] = 0.1
                                s['AUTOTHROTTLE_MAX_DELAY'] = 0.5
                                s['CONCURRENT_REQUESTS'] = 100
                                s['REACTOR_THREADPOOL_MAXSIZE'] = 20
                                s['DEPTH_PRIORITY'] = 1
                                s['SCHEDULER_DISK_QUEUE'] = 'scrapy.squeues.PickleFifoDiskQueue'
                                s['SCHEDULER_MEMORY_QUEUE'] = 'scrapy.squeues.FifoMemoryQueue'
                                process = CrawlerProcess(settings=s)
                                process.crawl(ProductListSpider, index_url=url, job_id=job_id)
                                # update_status_job(job_id=job_id, status='on process')
                                update_status_job_with_process_id(job_id=job_id, status='on process', process_id=my_pid)
                jobs = get_pending_jobs()
                if jobs:
                    if len(jobs) > 0:
                        job = jobs[0]
                        job_id = job.get("job_id", None)
                        url = job.get("url", None)
                        spider = job['spider']
                        if spider == 'BrandListSpider':
                            s = get_project_settings()
                            s['CONCURRENT_REQUESTS'] = 100
                            # s['REACTOR_THREADPOOL_MAXSIZE'] = 20
                            s['JOBDIR'] = jobdir % spider
                            s['AUTOTHROTTLE_ENABLED'] = True
                            s['AUTOTHROTTLE_START_DELAY'] = 0.1
                            s['AUTOTHROTTLE_MAX_DELAY'] = 0.5
                            # s['DEPTH_PRIORITY'] = 1
                            # s['SCHEDULER_DISK_QUEUE'] = 'scrapy.squeues.PickleFifoDiskQueue'
                            # s['SCHEDULER_MEMORY_QUEUE'] = 'scrapy.squeues.FifoMemoryQueue'
                            process = CrawlerProcess(settings=s)

                            # update_status_job(job_id=job_id, status='on process')
                            update_status_job_with_process_id(job_id=job_id, status='on process', process_id=my_pid)
                            if os.path.isdir("crawls"):
                                shutil.rmtree("crawls")
                                print("directory crawls has been removed")
                            process.crawl(BrandListSpider, index_url=url, job_id=job_id, jobdir=jobdir % spider)
                        elif spider == 'ProductListSpider':
                            if 'tokopedia.com' in url:
                                s = daruma_project_settings(jobdir=jobdir, spider=spider)
                                print('project_settings keys: %s' % [key for key in s.keys()])
                                print('DOWNLOADER_MIDDLEWARES: %s' % [i for i in s.get('DOWNLOADER_MIDDLEWARES')])
                                process = CrawlerProcess(settings=s)
                                if os.path.isdir("crawls"):
                                    shutil.rmtree("crawls")
                                    print("directory crawls has been removed")
                                process.crawl(ProductListSpiderSelenium, index_url=url, job_id=job_id,jobdir=jobdir % spider)
                                update_status_job_with_process_id(job_id=job_id, status='on process', process_id=my_pid)
                            else:
                                s = get_project_settings()
                                s['JOBDIR'] = jobdir % spider
                                s['AUTOTHROTTLE_ENABLED'] = True
                                s['AUTOTHROTTLE_START_DELAY'] = 0.1
                                s['AUTOTHROTTLE_MAX_DELAY'] = 0.5
                                s['CONCURRENT_REQUESTS'] = 100
                                # s['REACTOR_THREADPOOL_MAXSIZE'] = 20
                                # s['DEPTH_PRIORITY'] = 1
                                # s['SCHEDULER_DISK_QUEUE'] = 'scrapy.squeues.PickleFifoDiskQueue'
                                # s['SCHEDULER_MEMORY_QUEUE'] = 'scrapy.squeues.FifoMemoryQueue'
                                process = CrawlerProcess(settings=s)
                                process.crawl(ProductListSpider, index_url=url, job_id=job_id)
                                # update_status_job(job_id=job_id, status='on process')
                                update_status_job_with_process_id(job_id=job_id, status='on process', process_id=my_pid)
        current_job_id = get_job_id_by_process_id(process_id=my_pid)
        if current_job_id:
            try:
                now = datetime.now()
                start_time = get_start_time_by_job_id(job_id=current_job_id)
                subs = now - start_time
                min = divmod(subs.days * 86400 + subs.seconds, 60)[0]
                count_items = count_item_by_job_id(current_job_id)
                result = count_items / min
                logging.warning(">>>>>>>>>> SPEEED: %s/%s = %s" % (count_items, min, result))
                result = format(result, ".2f")
                update_speed_by_job_id(job_id=current_job_id, speed=str(result))
            except Exception:
                pass
        failed_jobs = get_failed_jobs()
        logging.warning("failed jobs: %s" % len(failed_jobs))
        if failed_jobs:
            for job in failed_jobs:
                delete_requests(id=job['job_id'])
                update_status_job_and_requests(job_id=job['job_id'], status='pending')
        logging.warning("no pending jobs in database")
        logging.warning("unit id: %s" % my_pid)
        time.sleep(5)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       




def run_flask():
    os.system("python app.py")


def run_app():
    s = threading.Thread(name="spider", target=run_spider)
    s.start()


if __name__ == "__main__":
    run_spider()
    # print(os.getcwd())
