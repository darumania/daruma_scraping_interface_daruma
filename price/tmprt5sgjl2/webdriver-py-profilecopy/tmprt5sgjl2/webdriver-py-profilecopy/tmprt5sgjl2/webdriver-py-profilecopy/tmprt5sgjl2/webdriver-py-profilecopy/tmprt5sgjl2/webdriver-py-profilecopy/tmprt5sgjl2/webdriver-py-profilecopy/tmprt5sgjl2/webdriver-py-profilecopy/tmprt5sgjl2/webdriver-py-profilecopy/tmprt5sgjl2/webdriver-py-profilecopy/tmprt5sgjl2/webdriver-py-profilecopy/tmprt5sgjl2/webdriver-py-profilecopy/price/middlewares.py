# -*- coding: utf-8 -*-

# Define here the models for your spider middleware
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/spider-middleware.html

from scrapy import signals
from scrapy.http import HtmlResponse
from scrapy.utils.python import to_bytes
from selenium import webdriver
import time
import os
from .spiders.product_list_selenium import ProductListSpiderSelenium
from .spiders.postgresql import *
from datetime import datetime

class PriceSpiderMiddleware(object):
    # Not all methods need to be defined. If a method is not defined,
    # scrapy acts as if the spider middleware does not modify the
    # passed objects.

    @classmethod
    def from_crawler(cls, crawler):
        # This method is used by Scrapy to create your spiders.
        s = cls()
        crawler.signals.connect(s.spider_opened, signal=signals.spider_opened)
        return s

    def process_spider_input(self, response, spider):
        # Called for each response that goes through the spider
        # middleware and into the spider.

        # Should return None or raise an exception.
        return None

    def process_spider_output(self, response, result, spider):
        # Called with the results returned from the Spider, after
        # it has processed the response.

        # Must return an iterable of Request, dict or Item objects.
        for i in result:
            yield i

    def process_spider_exception(self, response, exception, spider):
        # Called when a spider or process_spider_input() method
        # (from other spider middleware) raises an exception.

        # Should return either None or an iterable of Response, dict
        # or Item objects.
        pass

    def process_start_requests(self, start_requests, spider):
        # Called with the start requests of the spider, and works
        # similarly to the process_spider_output() method, except
        # that it doesn�t have a response associated.

        # Must return only requests (not items).
        for r in start_requests:
            yield r

    def spider_opened(self, spider):
        spider.logger.info('Spider opened: %s' % spider.name)


class PriceDownloaderMiddleware(object):
    # Not all methods need to be defined. If a method is not defined,
    # scrapy acts as if the downloader middleware does not modify the
    # passed objects.

    @classmethod
    def from_crawler(cls, crawler):
        # This method is used by Scrapy to create your spiders.
        s = cls()
        crawler.signals.connect(s.spider_opened, signal=signals.spider_opened)
        return s

    def process_request(self, request, spider):
        # Called for each request that goes through the downloader
        # middleware.

        # Must either:
        # - return None: continue processing this request
        # - or return a Response object
        # - or return a Request object
        # - or raise IgnoreRequest: process_exception() methods of
        #   installed downloader middleware will be called
        return None

    def process_response(self, request, response, spider):
        # Called with the response returned from the downloader.

        # Must either;
        # - return a Response object
        # - return a Request object
        # - or raise IgnoreRequest
        return response

    def process_exception(self, request, exception, spider):
        # Called when a download handler or a process_request()
        # (from other downloader middleware) raises an exception.

        # Must either:
        # - return None: continue processing this exception
        # - return a Response object: stops process_exception() chain
        # - return a Request object: stops process_exception() chain
        pass

    def spider_opened(self, spider):
        spider.logger.info('Spider opened: %s' % spider.name)


class SeleniumMiddleware(object):

    # def __init__(self):
    #     options = webdriver.FirefoxOptions()
    #     ff_profile = webdriver.FirefoxProfile(profile_directory='%s/' % os.getcwd())
    #     options.add_argument('-headless')
    #     ff_profile.set_preference("dom.disable_beforeunload", True)
    #     self.driver = webdriver.Firefox(firefox_options=options, executable_path='%s/geckodriver' % os.getcwd())

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        middleware = cls()
        crawler.signals.connect(middleware.spider_opened, signals.spider_opened)
        crawler.signals.connect(middleware.spider_error, signals.spider_error)
        crawler.signals.connect(middleware.spider_close, signals.spider_closed)
        return middleware

    def process_request(self, request, spider):

        def get_page_source(url):
            result = None
            try:
                options = webdriver.FirefoxOptions()
                options.add_argument('-headless')
                driver = webdriver.Firefox(firefox_options=options, executable_path='%s/geckodriver' % os.getcwd())
                driver.get(url)
                page = driver.page_source
                current_url = driver.current_url
                result = dict(page=page, current_url=current_url)
                driver.close()
                return result
            except:
                return result

        page_source = get_page_source(url=request.url)
        while True:
            if not page_source:
                print('MIDDLEWARE: page_source none, trying to get..')
                page_source = get_page_source(url=request.url)
            else:
                break
        body = to_bytes(page_source['page'])
        return HtmlResponse(url=page_source['current_url'], body=body, encoding='utf-8', request=request)

    def spider_opened(self, spider):
        spider.logger.info('MIDDLEWARE: SPIDER OPENED')
        options = webdriver.FirefoxOptions()
        ff_profile = webdriver.FirefoxProfile(profile_directory='%s/' % os.getcwd())
        options.add_argument('-headless')
        ff_profile.set_preference("dom.disable_beforeunload", True)
        self.driver = webdriver.Firefox(firefox_options=options, executable_path='%s/geckodriver' % os.getcwd())

    def spider_close(self, spider):
        spider.logger.info('MIDDLEWARE: SPIDER CLOSED')
        self.driver.quit()

    def spider_error(self, failure, spider, response):
        spider.logger.info('MIDDLEWARE:!!!!!!!!!!!! SPIDER ERROR !!!!!!!!!!!!')
        spider.logger.info(str(failure))
        error = "%s~ %s" % (str(failure.getErrorMessage()), str(failure.frames))
        update_request_for_exception(url=response.url, exception_content=error)
