import psycopg2 as pg
# import _constants
# from .db_config import config
# import datetime
import json
# import db_config

PRODUCTION = True

# read connection parameters
if PRODUCTION:
    params = {
        "host": "172.17.0.2",
        "database": "daruma_scraping",
        "user": "postgres",
        "password": "daruma123",
        "port": 5432
    }
else:
    params = {
        "host": "46.101.77.153",
        "database": "daruma_scraping",
        "user": "postgres",
        "password": "daruma123",
        "port": 5432
    }


def get_latest_job():
    """ query data from the vendors table """
    conn = None
    result = list()
    query = """
    SELECT * FROM job ORDER BY job_id DESC LIMIT 1
    """
    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query)

        rows = cur.fetchall()

        for row in rows:
            job_id = row[0]
            url = row[1]
            daruma_code = row[2] if row[2] is not None else "-"
            created_at = row[3].strftime('%m/%d/%Y %H:%M')
            created_by = row[4]
            start_time = row[5]
            end_time = row[6]
            error_count = row[7] if row[7] is not None else 0
            status = row[8]
            spider = row[9] if row[9] else "-"
            url_display = '%s..' % row[1][:50] if len(url) > 50 else url
            requests = row[11]
            requests_200 = row[12]
            priority = row[10]
            unit_id = row[14] if row[14] is not None else '-'
            speed = row[15] if row[15] is not None else 0
            repeat_days = row[16] if row[17] is not None else '-'
            schedule_time = row[17] if row[16] is not None else '-'
            result.append(dict(job_id=job_id,
                               url=url,
                               created_at=created_at,
                               daruma_code=daruma_code,
                               created_by=created_by,
                               start_time=start_time,
                               end_time=end_time,
                               error_count=error_count,
                               status=status,
                               url_display=url_display,
                               requests=requests,
                               requests_200=requests_200,
                               spider=spider,
                               priority=priority,
                               unit_id=unit_id,
                               speed=speed,
                               schedule_time=schedule_time,
                               repeat_days=repeat_days))

        # close the cursor
        cur.close()
        return result[0]
    except (Exception, pg.DatabaseError) as e:
        print("error get latest job: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def delete_dropped_item(item_id):
    """ insert vendor into vendors table """

    # sql = """
    # INSERT INTO request(job_id, url, http_status_code, content_type) VALUES(%s, %s, %s, %s)
    # """

    sql = """
    DELETE FROM dropped_item WHERE item_id=%s
    """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (item_id))

        # commit the changes
        conn.commit()
        print("item id %s was deleted.." % (item_id))

        # close the cursor
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()

def connect():
    """ Connect to the postgresql database server """
    conn = None
    try:

        # connect to the postgresql server
        print("Connecting to the postgresql database..")
        conn = pg.connect(**params)

        # create cursor
        cur = conn.cursor()

        # execute statement
        print("Postgresql Database Version: ")
        cur.execute("SELECT version()")

        # display the PostgreSQL database server version
        db_version = cur.fetchone()
        print(db_version)

        # close the cursor
        cur.close()
        print("Cursor closed..")

    except (Exception, pg.DatabaseError) as error:
        print("Oops: ", error)

    finally:
        # close the connection / conn
        if conn:
            conn.close()
            print("Connection closed..")

def create_tables():
    """ create tables """

    # commands = """
    #        CREATE TABLE users (
    #             user_id SERIAL PRIMARY KEY,
    #             username VARCHAR(50) UNIQUE NOT NULL,
    #             password VARCHAR(50)
    #         );
    #         """

    commands = """
                   CREATE TABLE item (
                        item_id SERIAL PRIMARY KEY,
                        price INTEGER,
                        url VARCHAR(250),
                        data VARCHAR,
                        created_at timestamp without time zone NOT NULL DEFAULT now(),
                        FOREIGN KEY (url) REFERENCES request (url)
                        ON UPDATE CASCADE ON DELETE CASCADE
                    )
                """

    commands = ("""
                       CREATE TABLE request (
                         job_id INTEGER,
                         url VARCHAR,
                         http_status_code VARCHAR(50),
                         content_type VARCHAR(150),
                         content_length VARCHAR(50),
                         created_at timestamp without time zone NOT NULL DEFAULT now(),
                         exception_content VARCHAR(250),
                         FOREIGN KEY (job_id) REFERENCES job (job_id)
                         ON UPDATE CASCADE ON DELETE CASCADE
                        )
                """,
                """
                                   CREATE TABLE item (
                                        item_id SERIAL PRIMARY KEY,
                                        price INTEGER,
                                        url VARCHAR(250),
                                        data VARCHAR,
                                        created_at timestamp without time zone NOT NULL DEFAULT now(),
                                        FOREIGN KEY (url) REFERENCES request (url)
                                        ON UPDATE CASCADE ON DELETE CASCADE
                                    )
                """
                )

    # commands = (
    #             """
    #                CREATE TABLE job (
    #                     job_id SERIAL PRIMARY KEY,
    #                     url VARCHAR(200),
    #                     daruma_code VARCHAR(50),
    #                     created_at timestamp without time zone NOT NULL DEFAULT now(),
    #                     created_by VARCHAR(150),
    #                     start_time VARCHAR(50),
    #                     end_time VARCHAR(50),
    #                     error_count INTEGER
    #                 )
    #             """,
    #             """
    #                CREATE TABLE request (
    #                     job_id INTEGER,
    #                     url VARCHAR,
    #                     http_status_code VARCHAR(50),
    #                     content_type VARCHAR(150),
    #                     content_length VARCHAR(50),
    #                     created_at timestamp without time zone NOT NULL DEFAULT now(),
    #                     exception_content VARCHAR(250),
    #                     FOREIGN KEY (job_id) REFERENCES job (job_id)
    #                     ON UPDATE CASCADE ON DELETE CASCADE
    #                 )
    #             """,
    #             """
    #                CREATE TABLE item (
    #                     item_id SERIAL PRIMARY KEY,
    #                     price INTEGER,
    #                     url VARCHAR(250),
    #                     data VARCHAR,
    #                     created_at timestamp without time zone NOT NULL DEFAULT now(),
    #                     FOREIGN KEY (url) REFERENCES request (url)
    #                     ON UPDATE CASCADE ON DELETE CASCADE
    #                 )
    #             """
    #             )

    # commands = (
    #     """
    #        CREATE TABLE request_quotes (
    #             job_id INTEGER,
    #             url VARCHAR,
    #             http_status_code VARCHAR(50),
    #             content_type VARCHAR(150),
    #             content_length VARCHAR(50),
    #             created_at timestamp without time zone NOT NULL DEFAULT now(),
    #             requested_at timestamp without time zone,
    #             exception_content VARCHAR(250),
    #             FOREIGN KEY (job_id) REFERENCES job (job_id)
    #             ON UPDATE CASCADE ON DELETE CASCADE
    #         )
    #     """
    # )

    conn = None

    try:
        # connect to the database
        conn = pg.connect(**params)
        # create cursor
        cur = conn.cursor()

        # create table one by one
        print("Creating table one by one, please wait..")
        for command in commands:
            cur.execute(command)
        print("All tables has been created..")
        # close cursor
        cur.close()

        # commit the changes
        conn.commit()
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close the connection
        if conn is not None:
            conn.close()

def count_request(job_id):
    """ query data from the vendors table """
    conn = None

    query = """
    select count(*) from request where job_id=%s
    """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (job_id,))

        result = cur.fetchone()


        # close the cursor
        cur.close()
        return result[0]
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def update_request_200_job(requests_count, job_id):
    sql = """
    UPDATE job SET requests_200=%s WHERE job_id=%s
    """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (requests_count, job_id))

        # commit the changes
        conn.commit()
        print("requests_200 %s job was updated.." % (job_id))

        # close the cursor
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()


def update_request_job(requests_count, job_id):
    sql = """
    UPDATE job SET requests=%s WHERE job_id=%s
    """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (requests_count, job_id))

        # commit the changes
        conn.commit()
        print("requests %s job was updated.." % (job_id))

        # close the cursor
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()

def count_request_200(job_id):
    """ query data from the vendors table """
    conn = None

    query = """
        select count(*) from request where http_status_code='200' and job_id = %s
        """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (job_id,))

        result = cur.fetchone()

        # close the cursor
        cur.close()
        return result[0]
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()


def insert_dropped_request(job_id, url):
    """ insert vendor into vendors table """


    sql = """
    INSERT INTO dropped_item(job_id, url) VALUES(%s, %s)
    """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (job_id, url))

        # commit the changes
        conn.commit()
        print("%s was inserted into dropped item table.." % (url))

        # close the cursor
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()


def get_dropped_item():
    """ query data from the vendors table """
    conn = None
    result = list()
    query = """
    SELECT * FROM dropped_item
    """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query)

        rows = cur.fetchall()

        for row in rows:
            item_id = row[0]
            job_id = row[1]
            url = row[2]
            result.append(dict(
                item_id=item_id,
                job_id=job_id,
                url=url,
            ))


        # close the cursor
        cur.close()
        return result
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def get_requests_200(job_id):
    """ query data from the vendors table """
    conn = None
    result = list()
    query = """
    SELECT * FROM request WHERE http_status_code='200' AND job_id = %s
    """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (job_id,))

        rows = cur.fetchall()

        count =  1

        for row in rows:
            job_id = row[0]
            url = row[1]
            http_status_code = row[2]
            content_type = row[3]
            content_length = row[4]
            created_at = row[5].strftime('%m/%d/%Y %H:%M') if row[5] is not None else "-"
            exception_content = row[6].split("~") if row[6] is not None else "-"
            result.append(dict(
                count=count,
                job_id=job_id,
                url=url,
                http_status_code=http_status_code,
                content_type=content_type,
                content_length=content_length,
                created_at=created_at,
                exception_content=exception_content
            ))
            count += 1


        # close the cursor
        cur.close()
        return result
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def get_requests_by_job_id(job_id):
    """ query data from the vendors table """
    conn = None
    result = list()
    query = """
    SELECT * FROM request WHERE job_id=%s
    """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (job_id,))

        rows = cur.fetchall()

        count =  1

        for row in rows:
            job_id = row[0]
            url = row[1]
            http_status_code = row[2]
            content_type = row[3]
            content_length = row[4]
            created_at = row[5].strftime('%m/%d/%Y %H:%M') if row[5] is not None else "-"
            exception_content = row[6].split("~") if row[6] is not None else "-"
            result.append(dict(
                count=count,
                job_id=job_id,
                url=url,
                http_status_code=http_status_code,
                content_type=content_type,
                content_length=content_length,
                created_at=created_at,
                exception_content=exception_content
            ))
            count += 1


        # close the cursor
        cur.close()
        return result
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def get_job_by_id(job_id):
    """ query data from the vendors table """
    conn = None
    result = list()
    query = """
    SELECT * FROM job WHERE job_id=%s ORDER BY job_id DESC
    """
    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (job_id,))

        rows = cur.fetchall()

        for row in rows:
            job_id = row[0]
            url = row[1]
            daruma_code = row[2] if row[2] is not None else "-"
            created_at = row[3].strftime('%m/%d/%Y %H:%M')
            created_by = row[4]
            start_time = row[5]
            end_time = row[6]
            error_count = row[7]
            status=row[8]
            spider = row[9] if row[9] else "-"
            url_display = '%s..' % row[1][:50] if len(url) > 50 else url
            requests = len(get_requests_by_job_id(job_id))
            requests_200 = len(get_requests_200(job_id))
            result.append(dict(job_id=job_id,
                               url=url,
                               created_at=created_at,
                               daruma_code=daruma_code,
                               created_by=created_by,
                               start_time=start_time,
                               end_time=end_time,
                               error_count=error_count,
                               status=status,
                               url_display=url_display,
                               requests = requests,
                               requests_200 = requests_200,
                               spider=spider))


        # close the cursor
        cur.close()
        return result[0]
    # except (Exception, pg.DatabaseError) as e:
    #     print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def get_cancel_col_job_by_id(job_id):
    """ query data from the vendors table """
    conn = None
    query = """
    SELECT cancel FROM job WHERE job_id=%s
    """
    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (job_id,))

        row = cur.fetchone()

        # close the cursor
        cur.close()
        return row[0]
    # except (Exception, pg.DatabaseError) as e:
    #     print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def insert_task(task_name, index_url=None, product_url=None, brand_filter=None, detail_filter=None, task_type=None, requested_by=str()):
    """ insert vendor into vendors table """

    sql = """
    INSERT INTO task(task_name, 
    index_url, 
    product_url, 
    brand_filter, 
    detail_filter, 
    task_type, 
    requested_by) 
    VALUES(%s, %s, %s, %s, %s, %s, %s)
    """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (task_name, index_url, product_url, brand_filter, detail_filter, task_type, requested_by,))

        # commit the changes
        conn.commit()
        print("%s was inserted into task table.." % (task_name))

        # close the cursor
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()


def insert_job(url, created_by, daruma_code, spider, priority, schedule_time, repeat_days=0):
    """ insert vendor into vendors table """

    sql = """
    INSERT INTO job(url, 
    created_by, daruma_code, spider, priority, schedule_time, repeat_days) 
    VALUES(%s, %s, %s, %s, %s, %s, %s)
    """

    # url = url[:int(str(url).find('?'))] if '?' in url else url

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (url, created_by, daruma_code, spider, priority, schedule_time, repeat_days))

        # commit the changes
        conn.commit()
        print("%s was inserted into job table.." % (url))

        # close the cursor
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()

def insert_request(job_id, url, http_status_code, content_type):
    """ insert vendor into vendors table """


    sql = """
    INSERT INTO request(job_id, url, http_status_code, content_type) VALUES(%s, %s, %s, %s)
    """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (job_id, url, http_status_code, content_type))

        # commit the changes
        conn.commit()
        print("%s was inserted into request table.." % (url))

        # close the cursor
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()

def get_requests_by_url(url, job_id):
    """ query data from the vendors table """
    conn = None
    result = list()
    query = """
    SELECT * FROM request WHERE url=%s AND job_id=%s
    """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (url, job_id,))

        rows = cur.fetchall()

        count =  1

        for row in rows:
            job_id = row[0]
            url = row[1]
            http_status_code = row[2]
            content_type = row[3]
            content_length = row[4]
            created_at = row[5].strftime('%m/%d/%Y %H:%M') if row[5] is not None else "-"
            exception_content = row[6].split("~") if row[6] is not None else "-"
            result.append(dict(
                count=count,
                job_id=job_id,
                url=url,
                http_status_code=http_status_code,
                content_type=content_type,
                content_length=content_length,
                created_at=created_at,
                exception_content=exception_content
            ))
            count += 1


        # close the cursor
        cur.close()
        if result:
            return result[0]
        else:
            return None
    # except (Exception, pg.DatabaseError) as e:
    #     print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def insert_item(url, job_id, data, daruma_code, spider):
    """ insert vendor into vendors table """

    sql = """
    INSERT INTO item(url, job_id, data, daruma_code, spider) VALUES(%s, %s, %s, %s, %s)
    """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (url, job_id, data, daruma_code, spider,))

        # commit the changes
        conn.commit()
        print("%s was inserted into item table.." % job_id)

        # close the cursor
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()


def update_request(url, status_code, content_type, job_id):
    """ insert vendor into vendors table """


    # sql = """
    # INSERT INTO request(job_id, url, http_status_code, content_type) VALUES(%s, %s, %s, %s)
    # """

    sql = """
    UPDATE request SET http_status_code=%s, content_type=%s WHERE url=%s AND job_id=%s
    """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (status_code, content_type, url, job_id,))

        # commit the changes
        conn.commit()
        print("%s was updated.." % (url))

        # close the cursor
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()

def update_request_for_exception(url, exception_content):
    """ insert vendor into vendors table """


    # sql = """
    # INSERT INTO request(job_id, url, http_status_code, content_type) VALUES(%s, %s, %s, %s)
    # """

    sql = """
    UPDATE request SET exception_content=%s WHERE url=%s
    """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (exception_content, url,))

        # commit the changes
        conn.commit()
        print("error content for request with url %s was updated.." % (url))

        # close the cursor
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()

# def update_status_job(job_id, status):
#
#     sql = """
#     UPDATE job SET status=%s WHERE job_id=%s
#     """
#
#     conn = None
#     try:
#         conn = pg.connect(**params)
#
#         # create new cursor
#         cur = conn.cursor()
#         # execute sql query
#         cur.execute(sql, (status, job_id,))
#
#         # commit the changes
#         conn.commit()
#         print("job %s status was updated.." % (job_id))
#
#         # close the cursor
#         cur.close()
#     except (Exception, pg.DatabaseError) as e:
#         print("oops: ", e)
#     finally:
#         # close conn
#         if conn is not None:
#             conn.close()

def update_status_job(job_id, status):

    sql = """
    UPDATE job SET status=%s, process_id=%s WHERE job_id=%s
    """
    process_id = '-'
    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (status, process_id, job_id,))

        # commit the changes
        conn.commit()
        print("job %s status was updated.." % (job_id))

        # close the cursor
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()

def update_starttime_job(job_id, start_time):
    """ insert vendor into vendors table """


    # sql = """
    # INSERT INTO request(job_id, url, http_status_code, content_type) VALUES(%s, %s, %s, %s)
    # """

    sql = """
    UPDATE job SET start_time=%s WHERE job_id=%s
    """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (start_time, job_id, ))

        # commit the changes
        conn.commit()
        print("start_time job %s was updated.." % (job_id))

        # close the cursor
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()

def update_endtime_job(job_id, end_time):
    """ insert vendor into vendors table """


    # sql = """
    # INSERT INTO request(job_id, url, http_status_code, content_type) VALUES(%s, %s, %s, %s)
    # """

    sql = """
    UPDATE job SET end_time=%s WHERE job_id=%s
    """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (end_time, job_id, ))

        # commit the changes
        conn.commit()
        print("end_time job %s was updated.." % (job_id))

        # close the cursor
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()


def insert_user(username, password):
    """ insert vendor into vendors table """

    sql = """
    INSERT INTO users(username, password) VALUES(%s, %s)
    """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (username, password,))

        # commit the changes
        conn.commit()
        print("%s with password %s was inserted into task table.." % (username, password))

        # close the cursor
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        return "error"
    finally:
        # close conn
        if conn is not None:
            conn.close()

def get_tasks():
    """ query data from the vendors table """
    conn = None
    result = list()
    query = """
    SELECT * FROM task
    """
    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query)

        rows = cur.fetchall()

        for row in rows:
            task_id = row[0]
            task_name = row[1]
            index_url = row[2]
            product_url = row[3]
            brand_filter = row[4]
            detail_filter = row[5]
            task_type = row[6]
            status = row[7]
            requested_by = row[8]
            d = dict(task_id=task_id,
                     task_name=task_name,
                     index_url=index_url,
                     product_url=product_url,
                     brand_filter=brand_filter,
                     detail_filter=detail_filter,
                     task_type=task_type,
                     status=status,
                     requested_by=requested_by)
            result.append(d)


        # close the cursor
        cur.close()
        return result
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def get_requests():
    """ query data from the vendors table """
    conn = None
    result = list()
    query = """
    SELECT * FROM request
    """

    # job_id
    # INTEGER,
    # url
    # VARCHAR,
    # http_status_code
    # VARCHAR(50),
    # content_type
    # VARCHAR(150),
    # content_length
    # VARCHAR(50),
    # created_at
    # timestamp
    # without
    # time
    # zone
    # NOT
    # NULL
    # DEFAULT
    # now(),
    # requested_at
    # timestamp
    # without
    # time
    # zone,
    # exception_content
    # VARCHAR(250),
    # FOREIGN
    # KEY(job_id)
    # REFERENCES
    # job(job_id)
    # ON
    # UPDATE
    # CASCADE
    # ON
    # DELETE
    # CASCADE

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query)

        rows = cur.fetchall()

        for row in rows:
            job_id = row[0]
            url = row[1]
            http_status_code = row[2]
            content_type = row[3]
            content_length = row[4]
            created_at = row[5].strftime('%m/%d/%Y %H:%M') if row[5] is not None else "-"
            exception_content = row[6]
            result.append(dict(
                job_id=job_id,
                url=url,
                http_status_code=http_status_code,
                content_type=content_type,
                content_length=content_length,
                created_at=created_at,
                exception_content=exception_content
            ))


        # close the cursor
        cur.close()
        return result
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()


def get_item():
    """ query data from the vendors table """
    conn = None
    result = list()
    query = """
    SELECT * FROM item
    """

    # item_id
    # SERIAL
    # PRIMARY
    # KEY,
    # job_id
    # INTEGER,
    # url
    # VARCHAR(250),
    # daruma_code
    # VARCHAR(100),
    # data
    # VARCHAR
    # NOT
    # NULL,
    # created_at
    # timestamp
    # without
    # time
    # zone
    # NOT
    # NULL
    # DEFAULT
    # now()

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query)

        rows = cur.fetchall()

        for row in rows:
            item_id = row[0]
            job_id = row[1]
            url = row[2]
            daruma_code=row[3]
            data = row[4]
            # created_at = row[3].strftime('%m/%d/%Y %H:%M') if row[5] is not None else "-"
            result.append(dict(
                item_id=item_id,
                job_id=job_id,
                url=url,
                data=data,
                daruma_code=daruma_code
                # created_at=created_at
            ))


        # close the cursor
        cur.close()
        return result
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()


def get_jobs():

    conn = None
    result = list()
    query = """
    SELECT * FROM job
    """
    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query)

        rows = cur.fetchall()

        for row in rows:
            job_id = row[0]
            url = row[1]
            created_at = row[2]
            created_by = row[3]
            start_time = row[4]
            end_time = row[5]
            error_count = row[6]
            priority = row[7]
            result.append(dict(job_id=job_id,
                               url=url,
                               created_at=created_at,
                               created_by=created_by,
                               start_time=start_time,
                               end_time=end_time,
                               error_count=error_count,
                               priority=priority))


        # close the cursor
        cur.close()
        return result
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()


def insert_error_count(job_id):
    """ insert vendor into vendors table """

    error = int(get_error_count(job_id=job_id)) + 1

    sql = """
    UPDATE job SET error_count=%s WHERE job_id=%s
    """

    # UPDATE job SET start_time=%s WHERE job_id=%s

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (error, job_id,))

        # commit the changes
        conn.commit()
        print("%s error_count was updated into job table.." % (job_id))

        # close the cursor
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()

def get_error_count(job_id):
    """ query data from the vendors table """
    conn = None
    result = list()
    query = """
    SELECT error_count FROM job WHERE job_id=%s
    """
    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (job_id,))

        row = cur.fetchone()
        row = 0 if row[0] == None else row
        # close the cursor
        cur.close()
        return row
    # except (Exception, pg.DatabaseError) as e:
    #     print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()


def get_detail(id):
    """get detail of data by id"""
    conn = None
    query = """
    SELECT * FROM job WHERE job_id = %s
    """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (id,))

        details = cur.fetchall()
        dic = dict()
        for d in details:
            job_id = d[0]
            url = d[1]
            start_time = d[5]
            created_at = d[3].strftime('%m/%d/%Y %H:%M') if d[3] is not None else "-"
            created_by = d[4]
            end_time = d[6]
            error_count = d[7]
            response_duration = end_time - start_time if end_time is not None else "-"
            daruma_code = d[2]
            dic = dict(
                job_id=job_id,
                url=url,
                created_at=created_at,
                created_by=created_by,
                start_time=start_time if start_time is not None else "-",
                end_time=end_time if end_time is not None else "-",
                error_count=error_count if error_count is not None else "0",
                response_duration=response_duration,
                daruma_code=daruma_code
            )


        cur.close()
        return dic
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        if conn is not None:
            conn.close()


def delete_task(id):

    conn = None
    query = """
    DELETE FROM task WHERE task_id = %s
    """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()

        cur.execute(query, (id,))

        conn.commit()
        print("id %s was deleted" % id)
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("Oops: ", e)
    finally:
        if conn is not None:
            conn.close()

def delete_job(id):

    conn = None
    query = """
    DELETE FROM job WHERE job_id = %s
    """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()

        cur.execute(query, (id,))

        conn.commit()
        print("id %s was deleted" % id)
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("Oops: ", e)
    finally:
        if conn is not None:
            conn.close()

def delete_user(user_id):

    conn = None
    query = """
    DELETE FROM users WHERE user_id = %s
    """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()

        cur.execute(query, (user_id,))

        conn.commit()
        print("User with id %s was deleted" % user_id)
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("Oops: ", e)
    finally:
        if conn is not None:
            conn.close()

def get_users():
    """ query data from the vendors table """
    conn = None
    result = list()
    query = """
    SELECT user_id, username, password FROM users
    """
    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query)

        rows = cur.fetchall()

        for row in rows:
            user_id = row[0]
            username = row[1]
            password = row[2]
            d = dict(user_id=user_id, username=username, password=password)
            result.append(d)

        # close the cursor
        cur.close()
        return result
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def get_status(job_id):
    """get detail of data by id"""
    conn = None
    query = """
    SELECT status FROM job WHERE job_id = %s
    """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (job_id,))

        status = cur.fetchone()[0]


        cur.close()
        return status
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        if conn is not None:
            conn.close()

def check_username(username, password):
    """ query data from the vendors table """
    conn = None
    result = None
    query = """
    SELECT password FROM users WHERE username = %s
    """
    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (username,))

        result = cur.fetchone()[0]

        # close the cursor
        cur.close()
        if result == password:
            return "success"
        else:
            return "error"
    except (Exception, pg.DatabaseError) as e:
        return "error"
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def dropTable(tableName):

    conn = None
    query = "DROP TABLE IF EXISTS {}".format(tableName)
    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query)

        cur.close()
        conn.commit()
        print("%s table was deleted" % tableName)

    except (Exception, pg.DatabaseError) as e:
        print("Oops: ", e)
    finally:
        if conn is not None:
            conn.close()


if __name__ == "__main__":
    # connect()
    # dropTable("request")
    # dropTable("item")
    # create_tables()
    # print(get_status(9))
    # task_name = "Test"
    # print(get_requests_by_job_id(169)[0])
    print(get_cancel_col_job_by_id(job_id=175))

    # url = "https://www.perkakasku.com/mesin-blower-baterai-tool-only-bosch-gbl-18-v-li-pt086.html"
    # insert_task(task_name=task_name, index_url=url, task_type="brand_list")
    # print(get_tasks())
    # add = insert_user(username="farhan", password="daruma123")
    # print(add)
    # print(get_users())
    # print(check_username("faan", "daruma123"))

    # dropTable("item")
    # print(json.dumps(get_tasks(), indent=4))
    # print(json.dumps(get_detail(2), indent=4))
    # keys = [x for x in get_detail(1).keys()]
    # print(keys)
    # delete_task(5)
    # print(get_tasks())