# -*- coding: utf-8 -*-
import scrapy
import time
import csv
import re
from html2text import html2text
from urllib.parse import urlparse, urljoin
from html.parser import HTMLParser
from .postgresql import delete_job, insert_job, get_latest_job, update_request_job, update_request_200_job, count_request, count_request_200, insert_dropped_request, get_dropped_item, delete_dropped_item, get_requests_by_url, insert_request, update_request, update_request_for_exception, update_status_job, insert_item, \
    update_endtime_job, update_starttime_job, get_detail, get_status, insert_error_count, get_job_by_id, get_cancel_col_job_by_id
from scrapy import signals
import json
from datetime import datetime
from scrapy.spidermiddlewares.httperror import HttpError
from twisted.internet.error import DNSLookupError
from twisted.internet.error import TimeoutError
from selenium import webdriver
import selenium as se
from scrapy.exceptions import CloseSpider
import os
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary


class HTMLStripper(HTMLParser):
    def __init__(self):
        super().__init__()
        self.reset()
        self.strict = False
        self.convert_charrefs = True
        self.fed = []

    def handle_data(self, d):
        self.fed.append(d)

    def get_data(self):
        return ''.join(self.fed)


def strip_tags(html):
    s = HTMLStripper()
    s.feed(html)
    return s.get_data()

def delete_files_in_jobdir(mydir):
    filelist = [f for f in os.listdir(mydir)]
    for f in filelist:
        os.remove(os.path.join(mydir, f))


class ProductListSpiderSelenium(scrapy.Spider):
    name = 'product_list_selenium'
    brand_filter_list = None
    detail_filter_list = None
    job_id = None
    index_url = None
    product_url = None

    crawlera_enabled = True
    crawlera_apikey = "94e8812c737a4402b857c01033c8426b"

    # event handling with signal API scrapy
    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        spider = super(ProductListSpiderSelenium, cls).from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.request_scheduled, signal=signals.request_scheduled)
        crawler.signals.connect(spider.response_received, signal=signals.response_received)
        crawler.signals.connect(spider.item_scraped, signal=signals.item_scraped)
        crawler.signals.connect(spider.spider_error, signal=signals.spider_error)
        crawler.signals.connect(spider.spider_opened, signal=signals.spider_opened)
        crawler.signals.connect(spider.spider_closed, signal=signals.spider_closed)
        crawler.signals.connect(spider.request_dropped, signal=signals.request_dropped)
        return spider

    def spider_closed(self, spider):
        spider.logger.info('ENGINE %s ENDED' % spider.name)
        job_id = getattr(self, 'job_id', None)
        jobdir = getattr(self, 'jobdir', None)
        update_endtime_job(job_id=self.job_id, end_time=datetime.now())
        # status = get_status(job_id=job_id)
        # if status == 'on process':
        #     update_status_job(job_id=job_id, status="error")
        #     error = "%s~ %s" % ("no item scraped", "check your spider")
        #     update_request_for_exception(job_id=job_id, exception_content=error)
        update_status_job(job_id=job_id, status="done")



    def request_dropped(self, request, spider):
        job_id = getattr(self, 'job_id', None)
        print('$$$$$$$ REQUEST DROPPED URL: %s $$$$$$$' % request.url)
        insert_dropped_request(job_id=job_id, url=request.url)


    def spider_opened(self, spider):
        spider.logger.info('ENGINE %s STARTED' % spider.name)
        job_id = getattr(self, 'job_id', None)
        update_starttime_job(job_id=job_id, start_time=datetime.now())


    def request_scheduled(self, request, spider):
        job_id = getattr(self, 'job_id', None)
        content_type = request.headers.get('Referer', None).decode("utf-8") if request.headers.get(
                           'Referer', None) is not None else None
        url_request = request.url[:str(request.url).find('?')] if '?' in str(request.url) else str(request.url)
        spider.logger.info('******** REQUEST URL ********: %s' % url_request)
        print("status code request: %s" % request.meta.get('depth', None))
        print("content type: %s" % content_type)
        print("url request: %s" % url_request)
        insert_request(job_id=job_id, url=url_request, http_status_code=request.meta.get('depth', None),
                       content_type=content_type)
        requests = count_request(job_id=job_id)
        request_200 = count_request_200(job_id=job_id)
        update_request_200_job(requests_count=request_200, job_id=job_id)
        update_request_job(requests_count=requests, job_id=job_id)

    def response_received(self, response, spider):
        job_id = getattr(self, 'job_id', None)
        content_type = response.headers.get("Content-Type", None).decode("utf-8") if response.headers.get(
            "Content-Type", None) is not None else "-"
        url_response = response.url[:str(response.url).find('?')] if '?' in str(response.url) else str(response.url)
        spider.logger.info('<<<<<<<<< RESPONSE URL >>>>>>>>: %s' % url_response)
        print("status code response: %s" % response.status)
        print("content type response: %s" % content_type)
        print("response-url response: %s" % url_response)
        update_request(url=url_response, content_type=content_type, status_code=response.status, job_id=job_id)
        requests = count_request(job_id=job_id)
        request_200 = count_request_200(job_id=job_id)
        update_request_200_job(requests_count=request_200, job_id=job_id)
        update_request_job(requests_count=requests, job_id=job_id)

    def item_scraped(self, item, response, spider):
        spider.logger.info('------------ INSERTING ITEM DATA ------------')
        # daruma_code = get_detail(self.job_id).get("daruma_code", None)
        # job_id = get_detail(self.job_id).get("job_id", None)
        # item_url = response.url[:int(str(response.url).find('?'))] if '?' in str(response.url) else str(response.url)
        # insert_item(url=item_url, job_id=job_id, data=str(item), daruma_code=daruma_code, spider="ProductListSpider")
        spider.logger.info('------------ ITEM SCRAPED ------------: %s' % response.url)

    def spider_error(self, failure, spider, response):
        spider.logger.info('!!!!!!!!!!!! SPIDER ERROR !!!!!!!!!!!!')
        job_id = getattr(self, 'job_id', None)
        spider.logger.info(str(failure))
        error = "%s~ %s" % (str(failure.getErrorMessage()), str(failure.frames))
        update_request_for_exception(url=response.url, exception_content=error)

    def parse_httpbin(self, response):
        self.logger.error("Got successful response from %s" % response.url)

    def errback_httpbin(self, failure):
        self.logger.error(repr(failure))
        if failure.check(HttpError):
            request = failure.request
            self.logger.error('DNSLookupError on %s' % request.url)

        elif failure.check(TimeoutError):
            request = failure.request
            self.logger.error("Timeout error on %s" % request.url)


    def start_requests(self):
        index_url = getattr(self, 'index_url', None)
        product_url = getattr(self, 'product_url', None)
        brand_filter = getattr(self, 'brand_filter', None)
        detail_filter = getattr(self, 'detail_filter', None)
        job_id = getattr(self, 'job_id', None)
        self.job_id = job_id
        # options = se.webdriver.FirefoxOptions()
        # ff_profile = se.webdriver.FirefoxProfile(profile_directory='%s/' % os.getcwd())
        # options.add_argument('-headless')
        # ff_profile.set_preference("dom.disable_beforeunload", True)
        # self.driver = se.webdriver.Firefox(firefox_options=options, executable_path='%s/geckodriver' % os.getcwd())

        if brand_filter:
            self.brand_filter_list = [s.strip().lower() for s in re.split(',|;', brand_filter)]
        if detail_filter:
            self.detail_filter_list = [s.strip().lower() for s in re.split(',|;', detail_filter)]

        if 'daruma.co.id' in index_url:
            yield scrapy.Request(index_url, self.parse_product)

        if 'jd.id' in index_url:
            path = urlparse(index_url).path
            if path == '/product':
                yield scrapy.Request(index_url, self.parse_product)
            elif path == '/search':
                yield scrapy.Request(index_url, self.parse_jdid_index)

        if 'elevenia.co.id' in index_url:
            yield scrapy.Request(index_url, self.parse_product)

        if 'lazada' in index_url:
            yield scrapy.Request(index_url, self.parse_product)

        if 'tokopedia' in index_url:
            print("tokopedia in index url..")
            yield scrapy.Request(index_url, self.parse_tokopedia_index, dont_filter=True)
            # path = urlparse(index_url).path
            # if len(str(path).split('/')) == 2:
            #     yield scrapy.Request(index_url, self.parse_tokopedia_index)
            # else:
            #     yield scrapy.Request(index_url, self.parse_product)

        if 'bhinneka' in index_url:
            yield scrapy.Request(index_url, self.parse_product)

        if 'monotaro.id' in index_url:
            path = urlparse(index_url).path
            if len(str(path).strip().split('/')) == 3:
                yield scrapy.Request(index_url, self.parse_product)
            else:
                yield scrapy.Request(index_url, self.parse_monotaro_index)

        if 'klikmro.com' in index_url:
            path = urlparse(index_url).path
            if len(str(path).strip().split('/')) == 3:
                yield scrapy.Request(index_url, self.parse_klikmro_index)
            elif len(str(path).strip().split('/')) == 4:
                yield scrapy.Request(index_url, self.parse_klikmro_product)


        if 'bukalapak.com' in index_url:
            path = urlparse(index_url).path
            if str(path).split('/')[1] == 'p':
                yield scrapy.Request(index_url, self.parse_product)
            if path == '/products':
                yield scrapy.Request(index_url, self.parse_bukalapak_index)

        if 'perkakasku.com' in index_url:
            if index_url.endswith('.html'):
                yield scrapy.Request(index_url, self.parse_product)
            else:
                yield scrapy.Request(index_url, self.parse_perkakasku_index)

        if 'indoteknik.com' in index_url:
            if 'v1' in index_url:
                yield scrapy.Request(index_url, self.parse_product)
            if 'shop/search' in index_url:
                yield scrapy.Request(index_url, self.parse_indoteknik_index)

        # if index_url.endswith('.html'):
        #     yield scrapy.Request(index_url, self.parse_product)

        # if index_url is not None:
        #     yield scrapy.Request(index_url, self.parse_index)

        if product_url is not None:
            yield scrapy.Request(product_url, self.parse_product)

        if job_id is not None:
            self.job_id = job_id

    def parse_index(self, response):
        domain = urlparse(response.url).netloc
        if domain.startswith("www."):
            domain = domain[4:]

        if domain.lower() == "monotaro.id":
            return self.parse_monotaro_index(response)
        elif domain.lower() == "perkakasku.com":
            return self.parse_perkakasku_index(response)
        elif domain.lower() == "klikmro.com":
            return self.parse_klikmro_index(response)
        elif domain.lower() == "indoteknik.com":
            return self.parse_indoteknik_index(response)
        elif domain.lower() == "bukalapak.com":
            return self.parse_bukalapak_index(response)

    def parse_monotaro_index(self, response):
        # path = urlparse(response.url).path
        # if path == '' or path == '/' or path == '/corp_id' or path == '/corp_id/':
        #     # Home page
        #     for cat in response.css(".em-catalog-navigation > .level0"):
        #         cat_url = cat.css("a::attr(href)").extract_first()
        #         title = cat.css("span::text").extract_first()
        #
        #         yield scrapy.Request(cat_url, self.parse_monotaro_index)
        #
        #     return

        # # Check if we have brand filter
        # brand = response.meta.get("brand", None)
        # if self.brand_filter_list and brand is None:
        #     # Filter by brand
        #     brand_list = None
        #     subcat_list = None
        #
        #     section_list = response.css("#narrow-by-list > *")
        #     for idx, e in enumerate(section_list):
        #         section_name = e.css("dt .title::text").extract_first()
        #         if section_name:
        #             if section_name.lower() == "kategori":
        #                 subcat_list = section_list[idx + 1]
        #             elif section_name.lower() == "brand":
        #                 brand_list = section_list[idx + 1]
        #
        #     if brand_list:
        #         for b in brand_list.css("li"):
        #             brand_name = b.css("::attr(data-text)").extract_first().strip()
        #             if brand_name.lower() in self.brand_filter_list:
        #                 try:
        #                     # This will get unselected brand only
        #                     brand_url = b.css("a.amshopby-attr::attr(href)").extract_first().strip()
        #                     yield scrapy.Request(brand_url, self.parse_monotaro_index, meta={"brand": brand_name})
        #
        #                 except:
        #                     pass
        #
        #     return
        if len(response.css(".item-products").extract()) == 0:
            print("redirect to parse product with url: %s" % self.index_url)
            return self.parse_product(response)
        else:
            # Parse all products
            for item in response.css(".item-products"):
                product_url = item.css(".product-name a::attr(href)").extract_first()
                yield scrapy.Request(product_url, self.parse_product)

            # Next page
            next_url = response.css("a.next-page::attr(href)").extract_first()
            if next_url:
                yield scrapy.Request(next_url, self.parse_monotaro_index)

        # Try to parse it as product.
        # WARNING: Don't know why this never get called. To scrap product directly,
        # use 'product_url' argument

    def parse_tokopedia_index(self, response):
        # Parse all products
        print('this is tokopedia index')
        list_page = response.css(".grid-shop-product .product")
        if 'page' in str(response.url) and len(list_page) == 0:
            print("reparsing for index url %s" % response.url)
            yield scrapy.Request(response.url, self.parse_tokopedia_index, dont_filter=True)
        print('len of list_page: %s' % len(list_page))
        if len(list_page) > 0:
            print('parse as list page')
            for item in response.css(".grid-shop-product .product"):
                product_url = item.css("a::attr(href)").extract_first()
                print('item url: %s' % product_url)
                yield scrapy.Request(product_url, self.parse_tokopedia_product, dont_filter=True)

            p = response.css('.pagination ul li a::attr(href)').extract()
            icon = response.css('.pagination ul li a::text').extract()
            if p:
                if len(p) == 1:
                    if icon[0] == "«" or icon[0] == "❮":
                        pass
                    else:
                        next_url = p[0]
                        # yield response.follow(next_url, self.parse_tokopedia_index)
                        yield scrapy.Request(next_url, self.parse_tokopedia_index, dont_filter=True)
                elif len(p) > 1:
                    next_url = p[-1]
                    yield scrapy.Request(next_url, self.parse_tokopedia_index, dont_filter=True)


        else:
            print('parse %s as single product page' % response.url)
            # Try to parse it as product.
            self.parse_product(response)
            # print('list product not read')




    def parse_jdid_index(self, response):
        print("scraping jd.id list product")
        for item in response.css(".list-products-t ul.clearfix li div.item div.p-desc"):
            product_url = "%s%s" % ("https://", item.css("a::attr(href)").extract_first()[2:])
            yield scrapy.Request(product_url, self.parse_product)

        # Next page
        next_url = response.css("div.pagination a::attr(href)").extract_first()
        if next_url:
            next_url = "%s%s" % ("https://www.jd.id", next_url)
            yield response.follow(next_url, self.parse_product)
        # Try to parse it as product.
        # WARNING: Don't know why this never get called. To scrap product directly,
        # use 'product_url' argument
        # self.parse_product(response)

    def parse_perkakasku_index(self, response):
        path = urlparse(response.url).path
        if path == '' or path == '/':
            # Home page
            for cat in response.css(".category-wrapper .category-menu > li"):
                # Main menu
                cat_url = cat.css("a::attr(href)").extract_first()
                if cat_url and cat_url.startswith("etalase"):
                    if not cat_url.startswith("http"):
                        cat_url = urljoin(response.url, cat_url)
                    yield scrapy.Request(cat_url, self.parse_perkakasku_index)

            return

        # if path.endswith('.html'):
        #     return parse_perkakasku_product(response)

        # if path.endswith('.html'):
        #     yield scrapy.Request(response.meta.get("index_url", "https://www.perkakasku.com/batu-asah-pisau-tora-8-bt730.html"), self.parse_product)

        # Check if we have brand filter
        brand = response.meta.get("brand", None)
        if self.brand_filter_list and brand is None:

            brand_list = response.css(".merk optgroup")[0].css("option")

            if brand_list:
                cat_url = response.url

                for b in brand_list:
                    tag = b.css("::attr(value)").extract_first().strip()
                    tag = tag.split("|")[1]
                    brand_name = b.css("::text").extract_first().strip()

                    if brand_name.lower() in self.brand_filter_list:
                        i = cat_url.rfind("-")
                        brand_url = "{}-{}-{}".format(cat_url[:i].replace("etalase-", "jual-"), tag, cat_url[i + 1:])

                        yield scrapy.Request(brand_url, self.parse_perkakasku_index,
                                             meta={"brand": brand_name})

            return

        # Parse all products
        for item in response.css("div.product-list-wrapper ul.product-list li"):
            product_url = item.css("a::attr(href)").extract_first()

            if not product_url.startswith("http"):
                product_url = "https://www.perkakasku.com/" + product_url

            yield scrapy.Request(product_url, self.parse_product)

        # Next page
        next_url = None
        for a in response.css("div.product-nav .product-paging a"):
            i = a.css("i::attr(class)")
            if i and i.extract_first().strip() == "fa fa-angle-right":
                next_url = a.css("::attr(href)").extract_first().strip()
                break

        if next_url:
            if not next_url.startswith("http"):
                if next_url.startswith("/"):
                    next_url = "https://www.perkakasku.com" + next_url
                else:
                    next_url = "https://www.perkakasku.com/" + next_url

            yield scrapy.Request(next_url, self.parse_perkakasku_index)

        # Try to parse it as product.
        self.parse_product(response)

    def parse_klikmro_index(self, response):
        path = urlparse(response.url).path
        if path == '' or path == '/':
            # Home page
            for cat in response.css(".category-menu .menu-wrapper > ul > li"):
                # Main menu
                cat_url = cat.css("a::attr(href)").extract_first()
                title = cat.css("a > span.cat-name::text").extract_first()

                yield scrapy.Request(cat_url, self.parse_klikmro_index)

            return

        # Check if we have brand filter
        brand = response.meta.get("brand", None)
        if self.brand_filter_list and brand is None:
            # Filter by brand
            brand_list = None
            subcat_list = None

            brand_list = response.css("dd.brand li")

            if brand_list:
                for b in brand_list:
                    brand_name = b.css("::attr(data-text)")
                    if not brand_name:
                        continue
                    brand_name = brand_name.extract_first().strip()
                    if brand_name.lower() in self.brand_filter_list:
                        brand_url = b.css("a::attr(href)").extract_first().strip()
                        yield scrapy.Request(brand_url, self.parse_klikmro_index, meta={"brand": brand_name})

            return

        # Parse all products
        for item in response.css(".products-grid li.item"):
            product_url = item.css("a::attr(href)").extract_first()
            yield scrapy.Request(product_url, self.parse_product)

        # Next page
        next_url = response.css(".pages a.next::attr(href)").extract_first().strip()
        if next_url:
            yield scrapy.Request(next_url, self.parse_klikmro_index)

        # Try to parse it as product.
        self.parse_product(response)

    def parse_indoteknik_index(self, response):
        path = urlparse(response.url).path
        if path == '' or path == '/':
            # Home page
            for cat_url in response.css(".box-vertical-megamenus .vertical-menu-list li").css(
                    "a.parent::attr(href)").extract():
                if cat_url.startswith("http"):
                    yield scrapy.Request(cat_url, self.parse_indoteknik_index)

            return

        brand = response.meta.get("brand", None)
        if self.brand_filter_list and brand is None:
            # Do not support filter by brand
            pass

        # Parse all products
        for item in response.css("#search-results .product-list .product-container-search div:first-child"):
            product_url = item.css("a::attr(href)").extract_first()

            yield scrapy.Request(product_url, self.parse_product)

        # Next page
        next_url = None
        a = response.css("#search-results .product-list a.page-next")
        if a:
            next_url = a.css("::attr(href)").extract_first().strip()

        if next_url:
            yield scrapy.Request(next_url, self.parse_indoteknik_index)

        # Try to parse it as product.
        self.parse_product(response)

    def parse_product(self, response):
        print("hi! this is parse product for %s" % response.url)
        domain = urlparse(response.url).netloc
        if domain.startswith("www."):
            domain = domain[4:]

        if domain.lower() == "daruma.co.id":
            return self.parse_daruma_product(response)
        if domain.lower() == "monotaro.id":
            print("parse_product: parse to monotaro product..")
            return self.parse_monotaro_product(response)
        elif domain.lower() == "perkakasku.com":
            return self.parse_perkakasku_product(response)
        elif domain.lower() == "klikmro.com":
            return self.parse_klikmro_product(response)
        elif domain.lower() == "indoteknik.com":
            return self.parse_indoteknik_product(response)
        elif domain.lower() == "tokopedia.com":
            print('parse tokopedia product url: %s' % response.url)
            return self.parse_tokopedia_product(response)
        elif domain.lower() == "bukalapak.com":
            return self.parse_bukalapak_product(response)
        elif domain.lower() == "lazada.co.id":
            return self.parse_lazada_product(response)
        elif domain.lower() == "elevenia.co.id":
            return self.parse_elevenia_product(response)
        elif domain.lower() == "bhinneka.com":
            return self.parse_bhinneka_product(response)
        elif domain.lower() == "jd.id":
            return self.parse_jdid_product(response)
        else:
            print("Invalid domain:", domain)

    def parse_monotaro_product(self, response):

        print("parse_monotaro_product: parse monotaro")

        try:
            title = response.css(".product-view-detail").css(".product-name h1::text").extract_first().strip()
        except:
            # Not a product page
            return

        seller = "Monotaro.id"

        sub_products = response.css(".product-view-detail").css(".associatedproduct tbody tr")
        if len(sub_products) > 0:
            brand = response.css(".product-view-detail").css(".brandContainer a::text").extract_first().strip()

            # Fix this could return more than one entry per TH
            table_columns = [th.css("::text").extract_first().strip().lower() for th in response.css(".product-view-detail").css(".associatedproduct tfoot th")]
            # table_columns = [s.strip().lower() for s in response.css(".product-view-detail").css(".associatedproduct thead th::text").extract()]

            # Parse associated products
            for p in sub_products:
                cols = p.css("td")

                sku_col = cols[table_columns.index('sku number')]
                sku = sku_col.css('a::text').extract_first().strip()
                url = sku_col.css('a::attr(href)').extract_first().strip()

                # Scrap the product
                # yield scrapy.Request(url, self.parse_product)
                # continue

                model = ""
                mpn = ""
                supplier_code = ""
                availability = ""
                price = ""
                price_before_discount = ""

                try:
                    model = cols[table_columns.index('model number')].css('::text').extract_first().strip()
                except:
                    pass

                try:
                    supplier_code = cols[table_columns.index('order number')].css('::text').extract_first().strip()
                except:
                    pass

                try:
                    supplier_code = cols[table_columns.index('supplier order code')].css(
                        '::text').extract_first().strip()
                except:
                    pass

                try:
                    supplier_code = cols[table_columns.index('trusco order code')].css('::text').extract_first().strip()
                except:
                    pass

                try:
                    availability = cols[table_columns.index('estimasi pengiriman')].css(
                        '::text').extract_first().strip()
                except:
                    pass

                price = cols[table_columns.index('harga')].css('.final-price::text').extract_first().strip()
                price = price.replace("Rp", "").replace(".", "")
                price_before_discount = price

                try:
                    image_url = response.css('a.cloud-zoom::attr(href)').extract_first()
                except:
                    image_url = None

                try:
                    price_before_discount = cols[table_columns.index('harga normal')].css(
                        'span::text').extract_first().strip()
                    price_before_discount = price_before_discount.replace("Rp", "").replace(".", "")
                    if price_before_discount == "" or price_before_discount == "-":
                        price_before_discount = price
                except:
                    pass

                # for td in p.css("td"):
                #     t = td.css("::text").extract_first().strip()
                #     if "rp" in t.lower():
                #         price = t.replace("Rp", "").replace(".", "")
                #         break

                yield {
                    'url': url,
                    'sku': sku,
                    'title': title,
                    'price': price,
                    'price_before_discount': price_before_discount,
                    'brand': brand,
                    'model': model,
                    'mpn': mpn,
                    'supplier_code': supplier_code,
                    'availability': availability,
                    'seller': seller,
                    'image_url':image_url
                }
        else:
            print("starting to parse monotaro single product..")

            brand = response.css(".product-view-detail").css(
                ".brandContainer .brands a::text").extract_first().strip().strip()
            sku = response.css(".product-view-detail").css(
                ".brandContainer .sku .detail-content::text").extract_first().strip()

            price = ""
            price_before_discount = ""

            try:
                price = response.css(".product-essential .price-box .regular-price").css(".price::text").extract_first().strip()
                price = price.replace("Rp", "").replace(".", "")
                price_before_discount = price
            except:
                price = response.css(".product-essential .price-box .special-price").css(".price::text").extract_first().strip()
                price = price.replace("Rp", "").replace(".", "")
                price_before_discount = price

            try:
                price_before_discount = response.css(".product-essential .price-box .old-price").css(
                    ".price::text").extract_first().strip()
                price_before_discount = price_before_discount.replace("Rp", "").replace(".", "")
            except:
                pass

            # try:
            #     availability = response.css(".ready-stock::text").extract_first().strip()
            # except:
            #     availability = ""
            attr_names = [s.strip().lower() for s in response.css(
                "div.product-collateral .product-detail-block .content .attribute-name::text").extract()]
            attr_values = [s.strip() for s in response.css(
                "div.product-collateral .product-detail-block .content .attribute-value::text").extract()]

            mpn = ""
            model = ""
            supplier_code = ""

            try:
                i = attr_names.index("model number")
                model = attr_values[i]
            except:
                pass

            try:
                i = attr_names.index("trusco order code")
                supplier_code = attr_values[i]
            except:
                pass

            try:
                i = attr_names.index("supplier order code")
                supplier_code = attr_values[i]
            except:
                pass

            try:
                i = attr_names.index("order number")
                supplier_code = attr_values[i]
            except:
                pass

            yield {
                'url': response.url,
                'sku': sku,
                'title': title,
                'price': price,
                'price_before_discount': price_before_discount,
                'brand': brand,
                'mpn': mpn,
                'model': model,
                'supplier_code': supplier_code,
                'availability': "",
                'seller': seller,
            }

    def parse_perkakasku_product(self, response):

        try:
            title = response.css("div.product-main-box h1.product-title::text").extract_first().strip()
        except:
            # Not a product page
            return

        seller = "Perkakasku.com"

        price = response.css("div.product-main-box .product-desc .price-origin strong::text").extract_first().strip()
        price = price.replace("Rp.", "").replace(".", "").replace(",-", "").strip()

        price_before_discount = price
        try:
            price_before_discount = response.css(
                "div.product-main-box .product-desc .price-list strong::text").extract_first().strip()
            price_before_discount = price_before_discount.replace("Rp.", "").replace(".", "").replace(",-", "").strip()
        except:
            pass

        # Perkakasku can have special promo price
        try:
            price_promo = response.css(
                "div.product-main-box .product-desc .price-promo strong::text").extract_first().strip()
            price_promo = price_promo.replace("Rp.", "").replace(".", "").replace(",-", "").strip()
            if price_promo:
                price = price_promo
        except:
            pass

        availability = response.css("div.product-main-box .product-desc .status span::text").extract_first().strip()
        sku = ""
        brand = ""
        model = ""
        mpn = ""
        supplier_code = ""

        description = ""
        try:
            description = response.css("div.product-main-box #product-specs .product-sekilas").extract_first().strip()
            description = html2text(description)
        except:
            pass

        spec_names = [s.strip().lower() for s in
                      response.css("div.product-main-box #product-specs tr td:first-child strong::text").extract()]
        spec_values = [strip_tags(s).strip() for s in
                       response.css("div.product-main-box #product-specs tr td:last-child").extract()]

        attr_names = [s.strip().lower() for s in
                      response.css("div.product-main-box #product-info tr td:first-child strong::text").extract()]
        attr_values = [strip_tags(s).strip() for s in
                       response.css("div.product-main-box #product-info tr td:last-child").extract()]

        image_url = response.css('div.product-image div.main-image img::attr(src)').extract_first()

        try:
            sku = attr_values[attr_names.index("kode")]
        except:
            pass
        try:
            brand = attr_values[attr_names.index("merk")]
        except:
            pass
        try:
            model = attr_values[attr_names.index("tipe")]
        except:
            pass
        try:
            mpn = attr_values[attr_names.index("no. part produsen")]
        except:
            pass

        data = {
            'url': response.url[:int(str(response.url).find('?'))] if '?' in response.url else response.url,
            'title': title,
            'price_before_discount': price_before_discount,
            'price': price,
            'sku': sku,
            'brand': brand,
            'model': model,
            'mpn': mpn,
            'supplier_code': supplier_code,
            'availability': availability,
            'seller': seller,
            'image_url' : image_url
        }

        # Add all specs
        if self.detail_filter_list:
            # Already saved
            detail_to_skip = ['merk', 'kode', 'tipe', 'no. part produsen']

            if 'all' in self.detail_filter_list or 'description' in self.detail_filter_list:
                data["description"] = description
            if 'all' in self.detail_filter_list or 'spec' in self.detail_filter_list:
                for idx, name in enumerate(spec_names):
                    data[name] = spec_values[idx]
            if 'all' in self.detail_filter_list or 'info' in self.detail_filter_list:
                for idx, name in enumerate(attr_names):
                    if name in detail_to_skip:
                        continue
                    data[name] = attr_values[idx]

        yield data

    def parse_tokopedia_product(self, response):
        print('starting to parse %s' % response.url)
        try:
            title = response.css("div h1.rvm-product-title span::text").extract_first().strip()
        except:
            # Not a product page
            return
        shop_name = response.css('.rvm-merchat-name a span.inline-block::text').extract_first().strip()
        seller = "tokopedia.com/%s" % shop_name

        seller_url = response.css('.widget-chat-controller .rvm-merchat-name a::attr(href)').extract_first()

        price = response.css('div.rvm-price input::attr("value")').extract_first()

        price_before_discount = price
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
        availability = ""
        sku = ""
        brand = ""
        model = ""
        mpn = ""
        supplier_code = ""

        try:
            image_url = response.css('div.content-main-img img::attr(src)').extract_first()
        except:
            image_url = ""

        try:
            sales = response.css('.va-middle .item-sold-count::text').extract_first()
            sales = str(sales).replace("rb", '00').replace(",", "") if "," in sales else str(sales).replace("rb", '000')
        except:
            sales = '0'
        try:
            views = response.css('.va-middle .view-count::text').extract_first()
            views = str(views).replace("rb", '00').replace(",", "") if "," in views else str(views).replace("rb", '000')
        except:
            views = '0'
        condition = response.css('.rvm-product-info--item .inline-block .mt-5::text').extract()[2]
        reviews = response.css(".span4 .rate-accuracy .reviewsummary-rating-count::text").extract_first().replace(
            "Ulasan", "").strip("\xa0") if response.css(
            ".span4 .rate-accuracy .reviewsummary-rating-count::text").extract_first() is not None else response.css(
            ".span4 .rate-accuracy .reviewsummary-rating-count::text").extract_first()
        whistlist = ''
        try:
            discussions = response.css('#talk-tab .talk-container::text').extract_first().replace('Diskusi Produk ',
                                                                                                  '').strip('(').strip(
                ')')
        except:
            discussions = '0'
        try:
            transactions = response.css('.side-box-left .all-transaction-count::text').extract_first()
            transactions = str(transactions).replace("rb", '00').replace(",", "") if "," in transactions else str(transactions).replace("rb", '000')
        except:
            transactions = '0'

        try:
            description = response.css('.product-summary__content').extract_first()
            description = strip_tags(str(description).replace("<br>", " "))
        except:
            description = '-'

        stock = response.css(".stock-text-wrapper span.stock-text::text").extract_first()
        if int(stock) == 0:
            stock = 10000

        try:
            tokped_product_id = response.css('meta::attr(content)').extract()[28].replace("product/", "")
        except:
            tokped_product_id = None

        try:
            sku = ""
        except:
            pass
        try:
            brand = ""
        except:
            pass
        try:
            model = ""
        except:
            pass
        try:
            mpn = ""
        except:
            pass

        job_id = getattr(self, 'job_id', None)

        data = {
            'spider' : 'ProductListSpider',
            'job_id': job_id,
            'url':response.url[:int(str(response.url).find('?'))] if '?' in str(response.url) else str(response.url),
            'title': title,
            'price_before_discount': price_before_discount,
            'price': price,
            'sku': sku,
            'brand': brand,
            'model': model,
            'mpn': mpn,
            'supplier_code': supplier_code,
            'availability': availability,
            'seller': seller,
            'seller_url': seller_url,
            'sales': sales,
            'views': views,
            'condition': condition,
            'reviews': reviews,
            'discussions': discussions,
            'transactions': transactions,
            'description': description,
            'image_url': image_url,
            'stock':stock,
            'tokped_product_id': tokped_product_id
        }

        yield data

    def parse_bukalapak_product(self, response):

        try:
            title = response.css('.c-product-detail__name::text').extract_first().strip()
        except:
            # Not a product page
            return
        shop_name = response.css('.o-flag__body .c-user-identification__name::text').extract_first().strip()
        seller = "bukalapak.com/%s" % shop_name

        price = response.css('.js-product-detail-price .amount::text').extract_first().strip().replace(".", "")

        seller_url = "%s%s" % ('https://www.bukalapak.com', response.css('.c-user-identification .o-flag__body a::attr(href)').extract_first())

        price_before_discount = price

        availability = ""
        sku = ""
        brand = ""
        model = ""
        mpn = ""
        supplier_code = ""

        sales = response.css(".o-flag__body .o-layout .o-layout__item .c-deflist dd.qa-pd-sold-value::text").extract_first().strip("\n")
        views = response.css(".qa-pd-seen-value::text").extract_first().strip("\n")
        condition = response.css(".qa-pd-condition-value span.c-label::text").extract_first()
        favorited = response.css(".qa-pd-favorited-value::text").extract_first().strip("\n")
        reviews = response.css(".c-product-rating__count span::text").extract_first()

        description = ""
        try:
            description = response.css('.tab-content .tab-pane').extract_first().strip()
            description = html2text(description)
        except:
            pass


        try:
            sku = ""
        except:
            pass
        try:
            brand = ""
        except:
            pass
        try:
            model = ""
        except:
            pass
        try:
            mpn = ""
        except:
            pass

        data = {
            'url': response.url[:int(str(response.url).find('?'))] if '?' in response.url else response.url,
            'title': title,
            'price_before_discount': price_before_discount,
            'price': price,
            'sku': sku,
            'brand': brand,
            'model': model,
            'mpn': mpn,
            'supplier_code': supplier_code,
            'availability': availability,
            'seller': seller,
            'seller_url' : seller_url,
            'sales' : sales,
            'views' : views,
            'condition' : condition,
            'favorited' : favorited,
            'reviews' : reviews
        }

        yield data


    def parse_lazada_product(self, response):

        try:
            title = response.css('.pdp-product-title::text').extract_first().strip()
        except:
            # Not a product page
            return
        shop_name = response.css('.seller-name__detail .pdp-link::text').extract_first().strip()
        seller = "lazada.co.id/%s" % shop_name

        seller_url = response.css('.seller-name__detail a::attr(href)').extract_first().strip('/')

        price = response.css('.pdp-product-price .pdp-price::text').extract_first().strip().replace('Rp', '').replace('.','')

        price_before_discount = price

        availability = ""
        sku = ""
        brand = ""
        model = ""
        mpn = ""
        supplier_code = ""

        description = ""
        try:
            description = response.css('.tab-content .tab-pane').extract_first().strip()
            description = html2text(description)
        except:
            pass


        try:
            sku = ""
        except:
            pass
        try:
            brand = ""
        except:
            pass
        try:
            model = ""
        except:
            pass
        try:
            mpn = ""
        except:
            pass

        data = {
            'url': response.url[:int(str(response.url).find('?'))] if '?' in response.url else response.url,
            'title': title,
            'price_before_discount': price_before_discount,
            'price': price,
            'sku': sku,
            'brand': brand,
            'model': model,
            'mpn': mpn,
            'supplier_code': supplier_code,
            'availability': availability,
            'seller': seller,
            'seller_url' : seller_url
        }

        yield data

    def parse_elevenia_product(self, response):

        try:
            title = response.css('.titleArea .notranslate::text').extract_first().strip()
        except:
            # Not a product page
            return
        shop_name = response.css('.storeMeta .storeWrap .storeNm::text').extract_first().strip()
        seller = "elevenia.co.id/%s" % shop_name
        seller_url = "%s%s" % ('http://www.elevenia.co.id', response.css('.storeMeta .storeWrap a::attr(href)').extract_first())

        price = response.css('.productOrder dl dd .price::text').extract_first().replace('Rp', '').replace('.','').strip()

        price_before_discount = price

        availability = ""
        sku = ""
        brand = ""
        model = ""
        mpn = ""
        supplier_code = ""

        description = ""
        try:
            description = response.css('.tab-content .tab-pane').extract_first().strip()
            description = html2text(description)
        except:
            pass


        try:
            sku = ""
        except:
            pass
        try:
            brand = ""
        except:
            pass
        try:
            model = ""
        except:
            pass
        try:
            mpn = ""
        except:
            pass

        data = {
            'url': response.url[:int(str(response.url).find('?'))] if '?' in response.url else response.url,
            'title': title,
            'price_before_discount': price_before_discount,
            'price': price,
            'sku': sku,
            'brand': brand,
            'model': model,
            'mpn': mpn,
            'supplier_code': supplier_code,
            'availability': availability,
            'seller': seller,
            'seller_url' : seller_url
        }

        yield data

    def parse_jdid_product(self, response):

        self.driver.get(url=response.url)

        try:
            title = response.css('.p-info .tit span::text').extract_first().strip()
        except:
            # Not a product page
            return
        shop_name = self.driver.find_element_by_css_selector(css_selector='.title-right .clickable .name').text
        seller = "jd.id/%s" % shop_name

        price = self.driver.find_element_by_class_name(name='sale-price')
        price = price.text.replace('Rp', '').replace(',','').strip()

        price_before_discount = price

        availability = ""
        sku = ""
        brand = ""
        model = ""
        mpn = ""
        supplier_code = ""

        description = ""
        try:
            description = response.css('.tab-content .tab-pane').extract_first().strip()
            description = html2text(description)
        except:
            pass


        try:
            sku = ""
        except:
            pass
        try:
            brand = ""
        except:
            pass
        try:
            model = ""
        except:
            pass
        try:
            mpn = ""
        except:
            pass

        data = {
            'url': response.url[:int(str(response.url).find('?'))] if '?' in response.url else response.url,
            'title': title,
            'price_before_discount': price_before_discount,
            'price': price,
            'sku': sku,
            'brand': brand,
            'model': model,
            'mpn': mpn,
            'supplier_code': supplier_code,
            'availability': availability,
            'seller': seller,
        }

        yield data

    def parse_daruma_product(self, response):

        self.driver.get(url=response.url)

        try:
            title = self.driver.find_element_by_class_name(name='title-detail-page')
            title = title.text
        except:
            # Not a product page
            return

        seller = "daruma.co.id"

        price = self.driver.find_element_by_css_selector(css_selector='.price-detail-page')
        price = price.text.replace('Rp', '').replace(',','').replace('.', '').strip()

        price_before_discount = price

        availability = ""
        sku = ""
        brand = ""
        model = ""
        mpn = ""
        supplier_code = ""

        description = ""
        try:
            description = response.css('.tab-content .tab-pane').extract_first().strip()
            description = html2text(description)
        except:
            pass

        try:
            sku = ""
        except:
            pass
        try:
            brand = ""
        except:
            pass
        try:
            model = ""
        except:
            pass
        try:
            mpn = ""
        except:
            pass

        data = {
            'url': response.url[:int(str(response.url).find('?'))] if '?' in response.url else response.url,
            'title': title,
            'price_before_discount': price_before_discount,
            'price': price,
            'sku': sku,
            'brand': brand,
            'model': model,
            'mpn': mpn,
            'supplier_code': supplier_code,
            'availability': availability,
            'seller': seller,
            'seller_url' : 'https://daruma.co.id'
        }

        yield data

    def parse_bhinneka_product(self, response):

        try:
            title = response.css(".theiaStickySidebar .bt-typo-displaylarge::text").extract_first()
        except:
            # Not a product page
            return

        seller = "Bhinneka.com"

        price = response.css("#product-detail-price .bt-product__price-old::text").extract_first().replace('Rp','').replace(',','').strip()

        price_before_discount = price

        availability = ""
        sku = ""
        brand = ""
        model = ""
        mpn = ""
        supplier_code = ""

        description = ""
        try:
            description = response.css('.tab-content .tab-pane').extract_first().strip()
            description = html2text(description)
        except:
            pass


        try:
            sku = ""
        except:
            pass
        try:
            brand = ""
        except:
            pass
        try:
            model = ""
        except:
            pass
        try:
            mpn = ""
        except:
            pass

        data = {
            'url': response.url[:int(str(response.url).find('?'))] if '?' in response.url else response.url,
            'title': title,
            'price_before_discount': price_before_discount,
            'price': price,
            'sku': sku,
            'brand': brand,
            'model': model,
            'mpn': mpn,
            'supplier_code': supplier_code,
            'availability': availability,
            'seller': seller,
            'seller_url' : 'https://bhinneka.com'
        }

        yield data

    def parse_klikmro_product(self, response):

        try:
            title = response.css(".product-essential .product-name h1::text").extract_first().strip()
        except:
            # Not a product page
            return

        seller = "KlikMRO"

        brand = response.css(".product-essential .product-shop .brand a::text").extract_first()

        price = ""
        price_before_discount = ""

        try:
            image_url = response.css('div.product-img-box div.product-image div.product-image-view a::attr(href)').extract_first()
        except:
            image_url = ""

        try:
            price = response.css(".product-essential .price-box .regular-price").css(
                ".price::text").extract_first().strip()
            price = price.replace("Rp", "").replace(".", "")
            price_before_discount = price
        except:
            price = response.css(".product-essential .price-box .special-price").css(
                ".price::text").extract_first().strip()
            price = price.replace("Rp", "").replace(".", "")
            price_before_discount = price

            try:
                price_before_discount = response.css(".product-essential .price-box .old-price").css(
                    ".price::text").extract_first().strip()
                price_before_discount = price_before_discount.replace("Rp", "").replace(".", "")
            except:
                pass

        price = price.replace("Rp", "").replace(".", "").replace(",", "").strip()
        price_before_discount = price_before_discount.replace("Rp", "").replace(".", "").replace(",", "").strip()

        availability = response.css(".product-essential .availability .value::text").extract_first().strip()
        sku = ""
        mpn = ""
        model = ""
        supplier_code = ""

        attr_names = [s.strip().lower() for s in
                      response.css(".specifications .data-table tbody tr th::text").extract()]
        attr_values = [s.strip() for s in response.css(".specifications .data-table tbody tr td::text").extract()]

        try:
            sku = attr_values[attr_names.index("sku")]
        except:
            pass

        try:
            mpn = attr_values[attr_names.index("manufacturing number")]
        except:
            pass

        yield {
            'url': response.url,
            'sku': sku,
            'title': title,
            'price': price,
            'price_before_discount': price_before_discount,
            'brand': brand,
            'mpn': mpn,
            'model': model,
            'supplier_code': supplier_code,
            'availability': availability,
            'seller': seller,
            'seller_url': 'https://www.klikmro.com/',
            'image_url' : image_url
        }

    def parse_indoteknik_product(self, response):

        try:
            title = response.css("#product h1::text").extract_first().strip()
        except:
            # Not a product page
            return

        seller = "Indoteknik.com"

        price = ""
        price_before_discount = ""

        try:
            image_url = response.css('div.product-image div.product-full img::attr(src)').extract_first()
        except:
            image_url = ""

        try:
            price = response.css("#prihil").css("span::text").extract_first().strip()
            price = price.replace("Rp", "").replace(".", "").strip()
            price_before_discount = price
        except:
            pass

        availability = ""
        sku = ""
        mpn = ""
        model = ""
        supplier_code = ""
        brand = ""

        attr_names = [s.strip().lower() for s in
                      response.css("#product .form-option tr th:first-child::text").extract()]
        attr_values = [strip_tags(s).replace(": ", "").strip() for s in
                       response.css("#product .form-option tr th:nth-child(2)").extract()]
        print(attr_names, attr_values)

        try:
            sku = attr_values[attr_names.index("order number")]
        except:
            pass
        try:
            mpn = attr_values[attr_names.index("part number")]
        except:
            pass
        try:
            model = attr_values[attr_names.index("model")]
        except:
            pass
        try:
            brand = attr_values[attr_names.index("manufacture")]
        except:
            pass

        yield {
            'url': response.url,
            'sku': sku,
            'title': title,
            'price': price,
            'price_before_discount': price_before_discount,
            'brand': brand,
            'mpn': mpn,
            'model': model,
            'supplier_code': supplier_code,
            'availability': availability,
            'seller': seller,
            'seller_url': 'http://indoteknik.com',
            'image_url' : image_url
        }
