FROM ubuntu:18.04

MAINTAINER Farhan "farhan@daruma.co.id"

RUN apt-get update -y && apt-get install -y -q python3 python3-pip firefox

COPY ./requirements.txt /app/requirements.txt

WORKDIR /app

RUN pip3 install -r requirements.txt
RUN pip3 install elasticsearch

COPY . /app

ENTRYPOINT [ "python3" ]

CMD [ "app.py" ]