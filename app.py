from flask import Flask, flash, render_template, request, session, redirect, url_for, escape, jsonify, make_response, send_file
from postgresql import get_reviews_by_url, get_store_name_from_price_table, update_bulk_price, get_price_order_by_total_sales, get_price_order_by_views, get_price_order_by_sales, edit_aggregate, get_product_by_product_id, get_product, get_requests_error, update_status_cancel_job, insert_rating, get_itemdata_byurl, get_itemdata_byjobid, get_item_byjobid, get_item_productlist, \
    get_item_brandlist, download_price_as_csv, search_price, get_price_by_id, get_store, check_username, get_price, \
    insert_task, get_item, get_detail, update_status_job, delete_task, get_requests, get_users, delete_job, insert_user, delete_user, get_jobs, insert_job
import os
import csv
import _constants
from flask_paginate import Pagination, get_page_parameter, get_page_args
from command import auto_brand_name_insert_command, convert_aggregate_into_csv, create_product, search, update_all_link_id_price, update_all_unlink_price, get_first_value, update_current_link_id, is_valid, get_links_by_urls
from datetime import datetime
from werkzeug.utils import secure_filename

app = Flask(__name__)
app.secret_key = "random string"
app.config['UPLOAD_FOLDER'] = "%s/custom_data" % os.getcwd()

# index endpoit
@app.route("/tester")
def tester():
    return render_template('base-template.html')

# index endpoit
@app.route("/")
def index():
    if 'username' not in session:
        return redirect(url_for("login"))
    else:
        username = session['username']
        return redirect("/user/%s" % username)

# login endpoint
@app.route("/login", methods=['GET', 'POST'])
def login():
    if 'username' not in session:
        if request.method == 'POST':
            username = request.form['username']
            password = request.form['password']
            check = check_username(username, password)
            if check is "error":
                flash("Oops.. try again..")
            else:
                session['username'] = username
                return redirect("/user/%s" % username)
    else:
        return redirect(url_for('index'))
    return render_template('new_login.html')


# home endpoint
@app.route("/user/<string:username>")
def user(username):
    if 'username' in session:
        username_session = session['username']
        if username == username_session:

            data = get_jobs()
            page, per_page, offset = get_page_args(page_parameter='page',
                                                   per_page_parameter='per_page')
            def get_data(offset=0, per_page=10):
                return data[offset: offset + per_page]
            pagination_data = get_data(offset=offset, per_page=per_page)
            total = len(data)
            page = request.args.get(get_page_parameter(), type=int, default=1)
            pagination = Pagination(page=page, per_page=per_page, total=total,
                                    css_framework='bootstrap4', alignment="center")
            return render_template('homepage.html', data=pagination_data,
                           page=page,
                           per_page=per_page, pagination=pagination, username=username)
        return make_response(page_not_found(404))
    else:
        return redirect(url_for("login"))


# add endpoint
@app.route("/user/<string:username>/addjobs")
def addjobs(username):
    if 'username' in session:
        username_session = session['username']
        if username == username_session:
            return render_template('addjobs.html', username=username)
        else:
            return make_response(page_not_found(404))
    else:
        return redirect(url_for("login"))

# add user endpoint
@app.route("/user/<string:username>/adduser")
def adduser(username):
    if 'username' in session:
        username_session = session['username']
        if username == username_session:
            return render_template('add_user.html', username=username)
        else:
            return make_response(page_not_found(404))
    else:
        return redirect(url_for("login"))

# add endpoint
@app.route("/user/<string:username>/viewusers")
def viewusers(username):
    if 'username' in session:
        username_session = session['username']
        if username == username_session:
            users = get_users()
            return render_template('view_users.html', username=username, users=users)
        else:
            return make_response(page_not_found(404))
    else:
        return redirect(url_for("login"))


# detail job
@app.route("/user/<string:username>/detail/<int:id>")
def view_detail(username, id):
    if 'username' in session:
        username_session = session['username']
        if username == username_session:
            data = get_detail(id=id)
            requests = get_requests(job_id=id)

            # pagination requests
            page, per_page, offset = get_page_args(page_parameter='page',
                                                   per_page_parameter='per_page')

            def get_data(offset=0, per_page=10):
                return requests[offset: offset + per_page]

            pagination_data = get_data(offset=offset, per_page=per_page)
            total = len(requests)
            page = request.args.get(get_page_parameter(), type=int, default=1)
            pagination = Pagination(page=page, per_page=per_page, total=total,
                                    css_framework='bootstrap4', alignment="center")

            items = get_item_byjobid(job_id=id)
            items_len = len(items)

            # pagination items
            page_items, per_page_items, offset_items = get_page_args(page_parameter='page',
                                                   per_page_parameter='per_page')

            def get_data(offset=0, per_page=10):
                return items[offset: offset + per_page]

            pagination_data_items = get_data(offset=offset_items, per_page=per_page_items)
            total_items = len(items)
            pagination_items = Pagination(page=page_items, per_page=per_page_items, total=total_items,
                                    css_framework='bootstrap4', alignment="center")

            if data is not None:
                request_count = get_requests(job_id=id)[len(get_requests(job_id=id))-1]['count'] if \
                    len(get_requests(job_id=id)) is not 0 else "-"
                keys = [x for x in data.keys()]
                return render_template('job_detail.html',
                                       username=username_session,
                                       keys=keys,
                                       data=data,
                                       id=id,
                                       request_count=request_count,
                                       requests=pagination_data,
                                       items=pagination_data_items,
                                       items_len=items_len,
                                       page=page,
                                       per_page=per_page,
                                       pagination=pagination,
                                       page_items=page_items,
                                       per_page_items=per_page_items,
                                       pagination_items=pagination_items
                                       )
            else:
                return make_response(page_not_found(404))
        return make_response(page_not_found(404))
    else:
        return redirect(url_for("login"))

# error detail endpoint
@app.route("/user/<string:username>/error_detail/<int:id>")
def view_error_detail(username, id):
    if 'username' in session:
        username_session = session['username']
        if username == username_session:
            data = get_requests_error(job_id=id)
            if data is not None:
                return render_template('error_detail.html',
                                       username=username_session,
                                       data=data,
                                       id=id)
            else:
                return make_response(page_not_found(404))
        return make_response(page_not_found(404))
    else:
        return redirect(url_for("login"))

# items endpoint
@app.route("/user/<string:username>/items")
def view_items(username):
    if 'username' in session:
        username_session = session['username']
        if username == username_session:
            selector = request.args.get('spider', None)
            if selector == 'BrandListSpider':
                data = get_item_brandlist()
                page, per_page, offset = get_page_args(page_parameter='page',
                                                       per_page_parameter='per_page')

                def get_data(offset=0, per_page=10):
                    return data[offset: offset + per_page]

                pagination_data = get_data(offset=offset, per_page=per_page)
                total = len(data)
                page = request.args.get(get_page_parameter(), type=int, default=1)
                pagination = Pagination(page=page, per_page=per_page, total=total,
                                        css_framework='bootstrap4', alignment="center")
                return render_template('items.html', data=pagination_data,
                                       page=page,
                                       per_page=per_page, pagination=pagination, username=username, le=selector)
            if selector == 'ProductListSpider':
                data = get_item_productlist()
                page, per_page, offset = get_page_args(page_parameter='page',
                                                       per_page_parameter='per_page')

                def get_data(offset=0, per_page=10):
                    return data[offset: offset + per_page]

                pagination_data = get_data(offset=offset, per_page=per_page)
                total = len(data)
                page = request.args.get(get_page_parameter(), type=int, default=1)
                pagination = Pagination(page=page, per_page=per_page, total=total,
                                        css_framework='bootstrap4', alignment="center")
                return render_template('items.html', data=pagination_data,
                                       page=page,
                                       per_page=per_page, pagination=pagination, username=username, le=selector)
            elif selector == 'no filter':
                data = get_item()
                page, per_page, offset = get_page_args(page_parameter='page',
                                                       per_page_parameter='per_page')

                def get_data(offset=0, per_page=10):
                    return data[offset: offset + per_page]

                pagination_data = get_data(offset=offset, per_page=per_page)
                total = len(data)
                page = request.args.get(get_page_parameter(), type=int, default=1)
                pagination = Pagination(page=page, per_page=per_page, total=total,
                                        css_framework='bootstrap4', alignment="center")
                return render_template('items.html', data=pagination_data,
                                       page=page,
                                       per_page=per_page, pagination=pagination, username=username, le=selector)
            elif selector == None:
                data = get_item()
                page, per_page, offset = get_page_args(page_parameter='page',
                                                       per_page_parameter='per_page')

                def get_data(offset=0, per_page=10):
                    return data[offset: offset + per_page]

                pagination_data = get_data(offset=offset, per_page=per_page)
                total = len(data)
                page = request.args.get(get_page_parameter(), type=int, default=1)
                pagination = Pagination(page=page, per_page=per_page, total=total,
                                        css_framework='bootstrap4', alignment="center")
                return render_template('items.html', data=pagination_data,
                                       page=page,
                                       per_page=per_page, pagination=pagination, username=username, le="no filter")
        return make_response(page_not_found(404))
    else:
        return redirect(url_for("login"))

# aggregate endpoint
@app.route("/user/<string:username>/aggregate")
def aggregate(username):
    if 'username' in session:
        username_session = session['username']
        if username == username_session:

            data = get_product()
            page, per_page, offset = get_page_args(page_parameter='page',
                                                   per_page_parameter='per_page')
            def get_data(offset=0, per_page=10):
                return data[offset: offset + per_page]
            pagination_data = get_data(offset=offset, per_page=per_page)
            total = len(data)
            page = request.args.get(get_page_parameter(), type=int, default=1)
            pagination = Pagination(page=page, per_page=per_page, total=total,
                                    css_framework='bootstrap4', alignment="center")
            return render_template('aggregate.html', data=pagination_data,
                           page=page,
                           per_page=per_page, pagination=pagination, username=username)
        return make_response(page_not_found(404))
    else:
        return redirect(url_for("login"))



# price table endpoint
@app.route("/user/<string:username>/price", methods=['GET'])
def view_price(username):
    if 'username' in session:
        username_session = session['username']
        if username == username_session:
            keyword = request.args.get('keyword', None)
            sorter = request.args.get('sorter')
            store_sorter = request.args.get('store_sorter', None)
            if sorter or store_sorter:
                sorter = sorter if sorter is not 'no_filter' else None
                store_sorter = store_sorter if store_sorter is not 'no_filter' else None
                data = None
                if sorter == 'total sales':
                    data = get_price_order_by_total_sales(store_sorter)
                elif sorter == 'views':
                    data = get_price_order_by_views(store_sorter)
                elif sorter == 'sales':
                    data = get_price_order_by_sales(store_sorter)
                else:
                    if store_sorter == 'no_filter':
                        data = get_price()
                    else:
                        data = get_price(store_sorter)
                if data is not None:
                    page, per_page, offset = get_page_args(page_parameter='page',
                                                           per_page_parameter='per_page')

                    def get_data(offset=0, per_page=10):
                        return data[offset: offset + per_page]

                    store_names = get_store_name_from_price_table()

                    pagination_data = get_data(offset=offset, per_page=per_page)
                    total = len(data)
                    page = request.args.get(get_page_parameter(), type=int, default=1)
                    pagination = Pagination(page=page, per_page=per_page, total=total,
                                            css_framework='bootstrap4', alignment="center")
                    return render_template('price.html', data=pagination_data,
                                           page=page,
                                           per_page=per_page, pagination=pagination,
                                           username=username,
                                           sorter=sorter,
                                           store_names=store_names,
                                           store_sorter=store_sorter)
            if keyword:
                data = search_price(keyword=keyword)
                if data is not None:
                    page, per_page, offset = get_page_args(page_parameter='page',
                                                           per_page_parameter='per_page')

                    def get_data(offset=0, per_page=10):
                        return data[offset: offset + per_page]

                    pagination_data = get_data(offset=offset, per_page=per_page)
                    total = len(data)
                    page = request.args.get(get_page_parameter(), type=int, default=1)
                    pagination = Pagination(page=page, per_page=per_page, total=total,
                                            css_framework='bootstrap4', alignment="center")
                    return render_template('price.html',
                                           data=pagination_data,
                                           page=page,
                                           per_page=per_page,
                                           pagination=pagination,
                                           username=username,
                                           keyword=keyword)
                else:
                    return make_response(page_not_found(404))
            data = get_price()
            if data is not None:
                page, per_page, offset = get_page_args(page_parameter='page',
                                                       per_page_parameter='per_page')

                def get_data(offset=0, per_page=10):
                    return data[offset: offset + per_page]

                store_names = get_store_name_from_price_table()

                pagination_data = get_data(offset=offset, per_page=per_page)
                total = len(data)
                page = request.args.get(get_page_parameter(), type=int, default=1)
                pagination = Pagination(page=page, per_page=per_page, total=total,
                                        css_framework='bootstrap4', alignment="center")
                return render_template('price.html',
                                       data=pagination_data,
                                       page=page,
                                       per_page=per_page,
                                       pagination=pagination,
                                       username=username,
                                       keyword=keyword,
                                       store_names=store_names)
            else:
                return make_response(page_not_found(404))
        return make_response(page_not_found(404))
    else:
        return redirect(url_for("login"))

# price history endpoint
@app.route("/user/<string:username>/price/history")
def view_history_price(username):
    if 'username' in session:
        username_session = session['username']
        if username == username_session:
            params = str(request.args.get("url")).split(',')
            url = params[0]
            tokped_product_id = params[1]
            data = get_itemdata_byurl(url, tokped_product_id=tokped_product_id)
            if data is not None:
                page, per_page, offset = get_page_args(page_parameter='page',
                                                       per_page_parameter='per_page')

                def get_data(offset=0, per_page=10):
                    return data[offset: offset + per_page]

                pagination_data = get_data(offset=offset, per_page=per_page)
                total = len(data)
                page = request.args.get(get_page_parameter(), type=int, default=1)
                pagination = Pagination(page=page, per_page=per_page, total=total,
                                        css_framework='bootstrap4', alignment="center")

                return render_template('history_price.html', data=pagination_data,
                                       page=page,
                                       per_page=per_page, pagination=pagination, username=username, url=url)
        #     else:
        #         return make_response(page_not_found(404))
        # else:
        #     return make_response(page_not_found(404))
    else:
        return redirect(url_for("login"))


# add user endpoint
@app.route("/adduser", methods=['GET', 'POST'])
def add_user():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        adduser = insert_user(username, password)
        if adduser == 'error':
            flash("Oops.. please try again")
            return redirect(url_for('index'))
        else:
            flash("%s user was created" % username)
            return redirect(url_for('index'))


# delete job endpoint
@app.route("/user/<string:username>/delete/<int:id>")
def deletejob(username, id):
    if 'username' in session:
        username_session = session['username']
        if username == username_session:
            delete_job(id)
            return redirect("/user/%s" % username)
        return jsonify({"error"})
    else:
        return redirect(url_for("login"))

# edit aggregate
@app.route("/user/<string:username>/edit_aggregate/", methods=['post'])
def edit_aggregate_page(username):
    if 'username' in session:
        username_session = session['username']
        if username == username_session:
            product_id = request.form.get('product_id')
            name = request.form.get('edit_name')
            description = request.form.get('edit_description')
            brand = request.form.get('edit_brand_name')
            sku = request.form.get('edit_sku')
            supplier_code = request.form.get('edit_supplier_code')
            edit_aggregate(product_id=product_id,
                           name=name,
                           description=description,
                           brand=brand,
                           sku=sku,
                           supplier_code=supplier_code)

            return redirect(url_for('aggregate', username=username))
        return jsonify({"error"})
    else:
        return redirect(url_for("login"))

# delete user endpoint
@app.route("/user/<string:username>/delete_user/<int:id>")
def deleteuser(username, id):
    if 'username' in session:
        username_session = session['username']
        if username == username_session:
            delete_user(id)
            return render_template('view_users.html', username=username)
        return page_not_found(404)
    else:
        return redirect(url_for('login'))

# add product list endpoint
@app.route("/addproductlist", methods=['GET', 'POST'])
def addproductlist():
    if request.method == 'POST':
        username = session['username']
        task_name = request.form['task_name']
        index_url = request.form['index_url']
        product_url = request.form['product_url']
        brand_filter = request.form['brand_filter']
        detail_filter = request.form['detail_filter']
        requested_by = session['username']
        insert_task(task_name=task_name,
                    index_url=index_url,
                    product_url=product_url,
                    brand_filter=brand_filter,
                    detail_filter=detail_filter,
                    task_type="product_list",
                    requested_by=requested_by)
        return redirect('/user/%s' % username)

# insert job endpoint
@app.route("/insert_job", methods=['GET', 'POST'])
def insertjob():
    if request.method == 'POST':
        username = session['username']
        input = request.form['input']
        schedule_time = request.form['schedule_date'] if len(str(request.form['schedule_date'])) > 2 else None
        repeat_days = request.form['repeat_days'] if len(str(request.form['repeat_days'])) > 0 else 0
        daruma_code = request.form['daruma_code']
        spider = request.form['spider']
        urls = str(input).replace(" ", "").splitlines()
        created_by = session['username']
        priority = request.form.get('priority', False)
        for url in urls:
            insert_job(url=url,
                       created_by=created_by,
                       daruma_code=daruma_code,
                       spider=spider,
                       priority=priority,
                       schedule_time=schedule_time,
                       repeat_days=int(repeat_days))
        return redirect('/user/%s' % username)

# add brand list endpoint
@app.route("/addbrandlist", methods=['GET', 'POST'])
def addbrandlist():
    if request.method == 'POST':
        username = session['username']
        task_name = request.form['task_name']
        index_url = request.form['index_url']
        requested_by = session['username']
        insert_task(task_name=task_name, index_url=index_url, task_type="brand_list", requested_by=requested_by)
        return redirect('/user/%s' % username)

# cancel job endpoint
@app.route("/user/<string:username>/update/<int:job_id>")
def canceljob(username, job_id):
    if 'username' in session:
        username_session = session['username']
        if username == username_session:
            update_status_job(job_id=job_id, status="cancelled")
            update_status_cancel_job(job_id=job_id)
            return redirect("/user/%s" % username)
        return jsonify({"error"})
    else:
        return redirect(url_for("login"))

# detail store endpoint
@app.route("/user/<string:username>/detail_store/<string:price_id>", methods=['POST', 'GET'])
def view_detail_store(username, price_id):
    if 'username' in session:
        username_session = session['username']
        if username == username_session:
            store_detail = get_price_by_id(price_id=price_id)
            rating = request.form.get('rating', None)
            if rating:
                rating = request.form.get('rating')
                insert_rating(item_url=store_detail['item_url'], rating=rating)
                if store_detail is not None:
                    return redirect("/user/%s/detail_store/%s" % (username, price_id))
                else:
                    return make_response(page_not_found(404))
            else:
                reviews = get_reviews_by_url(item_url=store_detail['item_url'])
                store = get_store(item_url=store_detail['item_url'])
                keys = list(store_detail.keys())
                rating = store_detail.get('rating', '-')
                if store_detail is not None:
                    return render_template("price_detail_new.html",
                                           data=store_detail,
                                           keys=keys,
                                           username=username,
                                           store=store,
                                           price_id=price_id,
                                           reviews=reviews,
                                           rating=rating)
        return make_response(page_not_found(404))
    else:
        return redirect(url_for("login"))


# detail aggregate
@app.route("/user/<string:username>/aggregate_detail/<string:product_id>", methods=['POST', 'GET'])
def view_aggregate_detail(username, product_id):
    if 'username' in session:
        username_session = session['username']
        if username == username_session:
            product_detail = get_product_by_product_id(product_id)[0]
            return render_template('aggregate_detail.html', data=product_detail, username=username)
    else:
        return redirect(url_for("login"))

# input rating
@app.route("/user/<string:username>/input_rating/<string:price_id>", methods=['POST'])
def input_rating(username, price_id):
    if 'username' in session:
        username_session = session['username']
        if username == username_session:
            rating = request.form.get('rating')
            store_detail = get_price_by_id(price_id=price_id)
            store = get_store(item_url=store_detail['item_url'])
            insert_rating(item_url=store_detail['item_url'], rating=rating)
            keys = list(store_detail.keys())
            if store_detail is not None:
                return render_template("price_detail_new.html", data=store_detail, keys=keys, username=username, store=store)
            else:
                return make_response(page_not_found(404))
        return make_response(page_not_found(404))
    else:
        return redirect(url_for("login"))

# auto brand name
@app.route("/user/<string:username>/upload-data-custom", methods=['POST', 'GET'])
def auto_brand_name(username):
    if 'username' in session:
        username_session = session['username']
        if username == username_session:
            if request.method == 'POST':
                f = request.files['file']
                filename = f.filename
                if str(filename)[-4:] != ".csv":
                    return render_template("auto-brand-name.html", username=username, message="upload failed: please use .tsv format instead..")
                else:
                    filename = "update.csv"
                    f.save(os.path.join(app.config['UPLOAD_FOLDER'], secure_filename(filename)))
                    # auto_brand_name_insert_command()
                    filepath = os.path.join(app.config['UPLOAD_FOLDER'], filename)
                    update_bulk_price(filepath)
                    message = "price table has been updated"
                    return render_template("auto-brand-name.html", username=username, message=message)
            return render_template("auto-brand-name.html", username=username)
        return make_response(page_not_found(404))
    else:
        return redirect(url_for("login"))

# logout endpoint
@app.route("/logout")
def logout():
    session.pop('username', None)
    return redirect(url_for('index'))

# download price endpoint
@app.route("/user/<string:username>/return-files", methods=['GET'])
def return_file(username):
    if 'username' in session:
        username_session = session['username']
        if username == username_session:
            try:
                sorter = request.args.get('sorter', None)
                store_sorter = request.args.get('store_sorter', None)
                date_now = datetime.today().strftime("%d-%b-%Y")
                sorter = sorter if sorter is not 'no_filter' else None
                store_sorter = store_sorter if store_sorter is not 'no_filter' else None
                download_price_as_csv(sorter, store_sorter)
                return send_file("%s/%s" % (os.getcwd(), "price_table.csv"), attachment_filename="price_table_%s.csv" % date_now, as_attachment=True)
            except Exception as e:
                print("ERROR: ",  e)
                return make_response(page_not_found(404))
        else:
            return make_response(page_not_found(404))
    else:
        return redirect(url_for("login"))

# download item endpoint
@app.route("/user/<string:username>/return-items-files/<int:id>/")
def return_items_file(username, id):
    if 'username' in session:
        username_session = session['username']
        if username == username_session:
            items = get_itemdata_byjobid(job_id=id)
            keys = items[0].keys()
            date_now = datetime.today().strftime("%d-%b-%Y")
            with open('items.csv', 'w', encoding='utf-8') as outputfile:
                dict_writer = csv.DictWriter(outputfile, keys)
                dict_writer.writeheader()
                dict_writer.writerows(items)
                outputfile.close()
            return send_file("%s/%s" % (os.getcwd(), "items.csv"), attachment_filename="items-%s.csv" % date_now, as_attachment=True)
        else:
            return make_response(page_not_found(404))
    else:
        return redirect(url_for("login"))


# download aggregate endpoint
@app.route("/user/<string:username>/return-aggregate-files/")
def return_aggregate_file(username):
    if 'username' in session:
        username_session = session['username']
        if username == username_session:
            convert_aggregate_into_csv()
            date_now = datetime.today().strftime("%d-%b-%Y")
            return send_file("%s/%s" % (os.getcwd(), "aggregate.csv"), attachment_filename="aggregate-%s.csv" % date_now, as_attachment=True)
        else:
            return make_response(page_not_found(404))
    else:
        return redirect(url_for("login"))

# link price table endpoint
@app.route("/user/<string:username>/link", methods=['get'])
def view_price_link(username):
    if 'username' in session:
        username_session = session['username']
        if username == username_session:
            keyword = request.args.get('keyword', None)
            links = request.args.getlist('link', None)
            urls_for_link = [i.split('#')[0] for i in links]
            es_ids = [i.split('#')[1] for i in links]
            if keyword:
                if urls_for_link:
                    button = request.args.get('link_button')
                    if button == 'link_this':
                        valid = is_valid(get_links_by_urls(urls_for_link))
                        if valid:
                            first_val = get_first_value(urls_for_link)
                            if first_val:
                                update_current_link_id(urls_for_link, link_id=first_val, es_ids=es_ids)
                            else:
                                update_all_link_id_price(urls_for_link, es_ids=es_ids)
                            data = search(index_name='price', keyword=keyword)
                            page, per_page, offset = get_page_args(page_parameter='page',
                                                                   per_page_parameter='per_page')

                            def get_data(offset=0, per_page=10):
                                return data[offset: offset + per_page]

                            pagination_data = get_data(offset=offset, per_page=per_page)
                            total = len(data)
                            page = request.args.get(get_page_parameter(), type=int, default=1)
                            pagination = Pagination(page=page, per_page=per_page, total=total,
                                                    css_framework='bootstrap4', alignment="center")

                            return render_template('price_link.html', data=pagination_data,
                                                   page=page,
                                                   per_page=per_page, pagination=pagination, username=username, link=urls_for_link,
                                                   keyword=keyword, first_val=first_val, urls_for_link=urls_for_link, valid=valid,
                                                   title=keyword)
                        else:
                            data = search_price(keyword=keyword)
                            page, per_page, offset = get_page_args(page_parameter='page',
                                                                   per_page_parameter='per_page')

                            def get_data(offset=0, per_page=10):
                                return data[offset: offset + per_page]

                            pagination_data = get_data(offset=offset, per_page=per_page)
                            total = len(data)
                            page = request.args.get(get_page_parameter(), type=int, default=1)
                            pagination = Pagination(page=page, per_page=per_page, total=total,
                                                    css_framework='bootstrap4', alignment="center")

                            return render_template('price_link.html', data=pagination_data,
                                                   page=page,
                                                   per_page=per_page, pagination=pagination, username=username,
                                                   link=urls_for_link,
                                                   keyword=keyword, urls_for_link=urls_for_link,
                                                   valid=valid, title=keyword)
                    elif button == 'unlink_this':
                        if is_valid(get_links_by_urls(urls_for_link)):
                            update_all_unlink_price(urls_for_link, es_ids=es_ids)
                            data = search(index_name='price', keyword=keyword)
                            page, per_page, offset = get_page_args(page_parameter='page',
                                                                   per_page_parameter='per_page')

                            def get_data(offset=0, per_page=10):
                                return data[offset: offset + per_page]

                            pagination_data = get_data(offset=offset, per_page=per_page)
                            total = len(data)
                            page = request.args.get(get_page_parameter(), type=int, default=1)
                            pagination = Pagination(page=page, per_page=per_page, total=total,
                                                    css_framework='bootstrap4', alignment="center")
                            return render_template('price_link.html', data=pagination_data,
                                                   page=page,
                                                   per_page=per_page, pagination=pagination, username=username,
                                                   link=urls_for_link,
                                                   keyword=keyword, title=keyword)
                        else:
                            data = search(index_name='price', keyword=keyword)
                            page, per_page, offset = get_page_args(page_parameter='page',
                                                                   per_page_parameter='per_page')

                            def get_data(offset=0, per_page=10):
                                return data[offset: offset + per_page]

                            pagination_data = get_data(offset=offset, per_page=per_page)
                            total = len(data)
                            page = request.args.get(get_page_parameter(), type=int, default=1)
                            pagination = Pagination(page=page, per_page=per_page, total=total,
                                                    css_framework='bootstrap4', alignment="center")
                            return render_template('price_link.html', data=pagination_data,
                                                   page=page,
                                                   per_page=per_page, pagination=pagination, username=username,
                                                   link=urls_for_link,
                                                   keyword=keyword, title=keyword)
                data = search(index_name='price', keyword=keyword)
                if data is not None:
                    page, per_page, offset = get_page_args(page_parameter='page',
                                                           per_page_parameter='per_page')

                    def get_data(offset=0, per_page=10):
                        return data[offset: offset + per_page]

                    pagination_data = get_data(offset=offset, per_page=per_page)
                    total = len(data)
                    page = request.args.get(get_page_parameter(), type=int, default=1)
                    pagination = Pagination(page=page, per_page=per_page, total=total,
                                            css_framework='bootstrap4', alignment="center")
                    return render_template('price_link.html', data=pagination_data,
                                           page=page,
                                           per_page=per_page, pagination=pagination, username=username,
                                           keyword=keyword, title=keyword)
                else:
                    return make_response(page_not_found(404))
            else:
                return render_template('price_link.html', username=username)
        return make_response(page_not_found(404))
    else:
        return redirect(url_for("login"))


# link price table endpoint
@app.route("/user/<string:username>/search/<int:price_id>", methods=['get'])
def search_from_price_page(username, price_id):
    if 'username' in session:
        username_session = session['username']
        if username == username_session:
            keyword = request.args.get('keyword', None)
            links = request.args.getlist('link', None)
            urls_for_link = [i.split('#')[0] for i in links]
            es_ids = [i.split('#')[1] for i in links]
            if price_id:
                if urls_for_link:
                    button = request.args.get('link_button')
                    if button == 'link_this':
                        valid = is_valid(get_links_by_urls(urls_for_link))
                        if valid:
                            first_val = get_first_value(urls_for_link)
                            if first_val:
                                update_current_link_id(urls_for_link, link_id=first_val, es_ids=es_ids)
                            else:
                                update_all_link_id_price(urls_for_link, es_ids=es_ids)
                        header_price_item = get_price_by_id(price_id=price_id)
                        result_search = search(index_name='price', keyword=header_price_item.get('name'))
                        main_data_es_id = result_search[0]['es_id'] if result_search else 0
                        data = result_search[1:]
                        title = header_price_item['name']

                        page, per_page, offset = get_page_args(page_parameter='page',
                                                               per_page_parameter='per_page')

                        def get_data(offset=0, per_page=10):
                            return data[offset: offset + per_page]

                        pagination_data = get_data(offset=offset, per_page=per_page)
                        total = len(data)
                        page = request.args.get(get_page_parameter(), type=int, default=1)
                        pagination = Pagination(page=page, per_page=per_page, total=total,
                                                css_framework='bootstrap4', alignment="center")

                        return redirect(url_for('search_from_price_page', username=username, price_id=price_id))
                        # return render_template('main_link.html',
                        #                        data=pagination_data,
                        #                        page=page,
                        #                        per_page=per_page,
                        #                        pagination=pagination,
                        #                        username=username,
                        #                        link=urls_for_link,
                        #                        keyword=keyword,
                        #                        urls_for_link=urls_for_link,
                        #                        title=title,
                        #                        main_data=header_price_item,
                        #                        main_data_es_id=main_data_es_id
                        #                        )
                    elif button == 'unlink_this':
                        if is_valid(get_links_by_urls(urls_for_link)):
                            update_all_unlink_price(urls_for_link, es_ids=es_ids)
                        header_price_item = get_price_by_id(price_id=price_id)
                        result_search = search(index_name='price', keyword=header_price_item.get('name'))
                        main_data_es_id = result_search[0]['es_id'] if result_search else 0
                        data = result_search[1:]
                        title = header_price_item['name']
                        page, per_page, offset = get_page_args(page_parameter='page',
                                                               per_page_parameter='per_page')

                        def get_data(offset=0, per_page=10):
                            return data[offset: offset + per_page]

                        pagination_data = get_data(offset=offset, per_page=per_page)
                        total = len(data)
                        page = request.args.get(get_page_parameter(), type=int, default=1)
                        pagination = Pagination(page=page, per_page=per_page, total=total,
                                                css_framework='bootstrap4', alignment="center")
                        # return render_template('main_link.html',
                        #                        data=pagination_data,
                        #                        page=page,
                        #                        per_page=per_page,
                        #                        pagination=pagination,
                        #                        username=username,
                        #                        link=urls_for_link,
                        #                        keyword=keyword,
                        #                        urls_for_link=urls_for_link,
                        #                        title=title,
                        #                        main_data=header_price_item,
                        #                        main_data_es_id=main_data_es_id
                        #                        )
                        return redirect(url_for('search_from_price_page',  username=username, price_id=price_id))
                if keyword:
                    if urls_for_link:
                        button = request.args.get('link_button')
                        if button == 'link_this':
                            valid = is_valid(get_links_by_urls(urls_for_link))
                            if valid:
                                first_val = get_first_value(urls_for_link)
                                if first_val:
                                    update_current_link_id(urls_for_link, link_id=first_val, es_ids=es_ids)
                                else:
                                    update_all_link_id_price(urls_for_link, es_ids=es_ids)
                            header_price_item = get_price_by_id(price_id=price_id)
                            result_search = search(index_name='price', keyword=keyword)
                            main_data_es_id = result_search[0]['es_id'] if result_search else 0
                            data = result_search[1:]
                            title = header_price_item['name']

                            page, per_page, offset = get_page_args(page_parameter='page',
                                                                   per_page_parameter='per_page')

                            def get_data(offset=0, per_page=10):
                                return data[offset: offset + per_page]

                            pagination_data = get_data(offset=offset, per_page=per_page)
                            total = len(data)
                            page = request.args.get(get_page_parameter(), type=int, default=1)
                            pagination = Pagination(page=page, per_page=per_page, total=total,
                                                    css_framework='bootstrap4', alignment="center")

                            return render_template('main_link.html',
                                                   data=pagination_data,
                                                   page=page,
                                                   per_page=per_page,
                                                   pagination=pagination,
                                                   username=username,
                                                   link=urls_for_link,
                                                   keyword=keyword,
                                                   urls_for_link=urls_for_link,
                                                   title=title,
                                                   main_data=header_price_item,
                                                   main_data_es_id=main_data_es_id
                                                   )
                        elif button == 'unlink_this':
                            if is_valid(get_links_by_urls(urls_for_link)):
                                update_all_unlink_price(urls_for_link, es_ids=es_ids)
                            header_price_item = get_price_by_id(price_id=price_id)
                            result_search = search(index_name='price', keyword=keyword)
                            main_data_es_id = result_search[0]['es_id'] if result_search else 0
                            data = result_search[1:]
                            title = header_price_item['name']
                            page, per_page, offset = get_page_args(page_parameter='page',
                                                                   per_page_parameter='per_page')

                            def get_data(offset=0, per_page=10):
                                return data[offset: offset + per_page]

                            pagination_data = get_data(offset=offset, per_page=per_page)
                            total = len(data)
                            page = request.args.get(get_page_parameter(), type=int, default=1)
                            pagination = Pagination(page=page, per_page=per_page, total=total,
                                                    css_framework='bootstrap4', alignment="center")
                            return render_template('main_link.html',
                                                   data=pagination_data,
                                                   page=page,
                                                   per_page=per_page,
                                                   pagination=pagination,
                                                   username=username,
                                                   link=urls_for_link,
                                                   keyword=keyword,
                                                   urls_for_link=urls_for_link,
                                                   title=title,
                                                   main_data=header_price_item,
                                                   main_data_es_id=main_data_es_id
                                                   )
                    header_price_item = get_price_by_id(price_id=price_id)
                    result_search = search(index_name='price', keyword=keyword)
                    main_data_es_id = result_search[0]['es_id'] if result_search else 0
                    data = result_search[1:]
                    title = header_price_item['name']
                    if data is not None:
                        page, per_page, offset = get_page_args(page_parameter='page',
                                                               per_page_parameter='per_page')

                        def get_data(offset=0, per_page=10):
                            return data[offset: offset + per_page]

                        pagination_data = get_data(offset=offset, per_page=per_page)
                        total = len(data)
                        page = request.args.get(get_page_parameter(), type=int, default=1)
                        pagination = Pagination(page=page, per_page=per_page, total=total,
                                                css_framework='bootstrap4', alignment="center")
                        return render_template('main_link.html',
                                               data=pagination_data,
                                               page=page,
                                               per_page=per_page,
                                               pagination=pagination,
                                               username=username,
                                               link=urls_for_link,
                                               keyword=keyword,
                                               urls_for_link=urls_for_link,
                                               title=title,
                                               main_data=header_price_item,
                                               main_data_es_id=main_data_es_id
                                               )
                header_price_item = get_price_by_id(price_id=price_id)
                result_search = search(index_name='price', keyword=header_price_item.get('name'))
                main_data_es_id = result_search[0]['es_id'] if result_search else 0
                data = result_search[1:]
                title = header_price_item['name']
                if data is not None:
                    page, per_page, offset = get_page_args(page_parameter='page',
                                                           per_page_parameter='per_page')

                    def get_data(offset=0, per_page=10):
                        return data[offset: offset + per_page]

                    pagination_data = get_data(offset=offset, per_page=per_page)
                    total = len(data)
                    page = request.args.get(get_page_parameter(), type=int, default=1)
                    pagination = Pagination(page=page, per_page=per_page, total=total,
                                            css_framework='bootstrap4', alignment="center")
                    return render_template('main_link.html',
                                           data=pagination_data,
                                           page=page,
                                           per_page=per_page,
                                           pagination=pagination,
                                           username=username,
                                           link=urls_for_link,
                                           keyword=keyword,
                                           urls_for_link=urls_for_link,
                                           title=title,
                                           main_data=header_price_item,
                                           main_data_es_id=main_data_es_id
                                           )
                else:
                    return make_response(page_not_found(404))
            else:
                return render_template('main_link.html', username=username)
        return make_response(page_not_found(404))
    else:
        return redirect(url_for("login"))


# error handler 404
@app.errorhandler(404)
def page_not_found(e):
    return render_template("404.html"), 404


if __name__ == "__main__":
    if _constants.PRODUCTION == False:
        app.run(debug=True)
    else:
        port = int(os.environ.get('PORT', 5000))
        app.run(host='0.0.0.0', port=port, debug=True)
        # app.run(debug=True)
