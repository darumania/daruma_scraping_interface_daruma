import psycopg2 as pg
from db_config import config
import json

# read connection parameters
params = config()

def connect():
    """ Connect to the postgresql database server """
    conn = None
    try:

        # connect to the postgresql server
        print("Connecting to the postgresql database..")
        conn = pg.connect(**params)

        # create cursor
        cur = conn.cursor()

        # execute statement
        print("Postgresql Database Version: ")
        cur.execute("SELECT version()")

        # display the PostgreSQL database server version
        db_version = cur.fetchone()
        print(db_version)

        # close the cursor
        cur.close()
        print("Cursor closed..")

    except (Exception, pg.DatabaseError) as error:
        print("Oops: ", error)

    finally:
        # close the connection / conn
        if conn:
            conn.close()
            print("Connection closed..")

def create_tables():
    """ create tables """

    # commands = """
    #        CREATE TABLE users (
    #             user_id SERIAL PRIMARY KEY,
    #             username VARCHAR(50) UNIQUE NOT NULL,
    #             password VARCHAR(50)
    #         );
    #         """

    # commands = """
    #            CREATE TABLE task (
    #                 task_id SERIAL PRIMARY KEY,
    #                 task_name VARCHAR(255) UNIQUE NOT NULL,
    #                 index_url VARCHAR(255),
    #                 product_url VARCHAR(255),
    #                 brand_filter VARCHAR(200),
    #                 detail_filter VARCHAR(255),
    #                 task_type VARCHAR(50),
    #                 status VARCHAR(50) DEFAULT 'incomplete',
    #                 requested_by VARCHAR(100)
    #             )
    #             """

    commands = (
                """
                   CREATE TABLE job (
                        job_id SERIAL PRIMARY KEY,
                        url VARCHAR(200),
                        created_at timestamp without time zone NOT NULL DEFAULT now(),
                        created_by VARCHAR(150),
                        start_time VARCHAR(50),
                        end_time VARCHAR(50),
                        error_count INTEGER
                    )
                """,
                """
                   CREATE TABLE request (
                        job_id INTEGER,
                        url VARCHAR,
                        http_status_code VARCHAR(50),
                        content_type VARCHAR(150),
                        content_length VARCHAR(50),
                        created_at timestamp without time zone NOT NULL DEFAULT now(),
                        requested_at timestamp without time zone,
                        exception_content VARCHAR(250),
                        FOREIGN KEY (job_id) REFERENCES job (job_id)
                        ON UPDATE CASCADE ON DELETE CASCADE
                    )
                """,
                """
                   CREATE TABLE item (
                        item_id SERIAL PRIMARY KEY,
                        price INTEGER,
                        url VARCHAR(250),
                        data VARCHAR,
                        created_at timestamp without time zone NOT NULL DEFAULT now(),
                        FOREIGN KEY (url) REFERENCES request (url)
                        ON UPDATE CASCADE ON DELETE CASCADE
                    )
                """
                )

    commands = (
        """
           CREATE TABLE request_quotes (
                job_id INTEGER,
                url VARCHAR,
                http_status_code VARCHAR(50),
                content_type VARCHAR(150),
                content_length VARCHAR(50),
                created_at timestamp without time zone NOT NULL DEFAULT now(),
                requested_at timestamp without time zone,
                exception_content VARCHAR(250),
                FOREIGN KEY (job_id) REFERENCES job (job_id)
                ON UPDATE CASCADE ON DELETE CASCADE
            )
        """
    )

    conn = None

    try:
        # connect to the database
        conn = pg.connect(**params)
        # create cursor
        cur = conn.cursor()

        # create table one by one
        print("Creating table one by one, please wait..")
        for command in commands:
            cur.execute(command)
        print("All tables has been created..")
        # close cursor
        cur.close()

        # commit the changes
        conn.commit()
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close the connection
        if conn is not None:
            conn.close()

def insert_task(task_name, index_url=None, product_url=None, brand_filter=None, detail_filter=None, task_type=None, requested_by=str()):
    """ insert vendor into vendors table """

    sql = """
    INSERT INTO task(task_name, 
    index_url, 
    product_url, 
    brand_filter, 
    detail_filter, 
    task_type, 
    requested_by) 
    VALUES(%s, %s, %s, %s, %s, %s, %s)
    """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (task_name, index_url, product_url, brand_filter, detail_filter, task_type, requested_by,))

        # commit the changes
        conn.commit()
        print("%s was inserted into task table.." % (task_name))

        # close the cursor
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()


def insert_job(url, created_by):
    """ insert vendor into vendors table """

    sql = """
    INSERT INTO job(url, 
    created_by) 
    VALUES(%s, %s)
    """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (url, created_by))

        # commit the changes
        conn.commit()
        print("%s was inserted into task table.." % (url))

        # close the cursor
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()

def insert_request(job_id, url, http_status_code, content_type):
    """ insert vendor into vendors table """


    sql = """
    INSERT INTO request(job_id, url, http_status_code, content_type) VALUES(%s, %s, %s, %s)
    """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (job_id, url, http_status_code, content_type))

        # commit the changes
        conn.commit()
        print("%s was inserted into request table.." % (url))

        # close the cursor
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()


def update_request(url, requested_at):
    """ insert vendor into vendors table """


    # sql = """
    # INSERT INTO request(job_id, url, http_status_code, content_type) VALUES(%s, %s, %s, %s)
    # """

    sql = """
    UPDATE request SET requested_at=%s WHERE url=%s
    """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (requested_at,url,))

        # commit the changes
        conn.commit()
        print("%s was updated.." % (url))

        # close the cursor
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()



def insert_user(username, password):
    """ insert vendor into vendors table """

    sql = """
    INSERT INTO users(username, password) VALUES(%s, %s)
    """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (username, password,))

        # commit the changes
        conn.commit()
        print("%s with password %s was inserted into task table.." % (username, password))

        # close the cursor
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        return "error"
    finally:
        # close conn
        if conn is not None:
            conn.close()

def get_tasks():
    """ query data from the vendors table """
    conn = None
    result = list()
    query = """
    SELECT * FROM task
    """
    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query)

        rows = cur.fetchall()

        for row in rows:
            task_id = row[0]
            task_name = row[1]
            index_url = row[2]
            product_url = row[3]
            brand_filter = row[4]
            detail_filter = row[5]
            task_type = row[6]
            status = row[7]
            requested_by = row[8]
            d = dict(task_id=task_id,
                     task_name=task_name,
                     index_url=index_url,
                     product_url=product_url,
                     brand_filter=brand_filter,
                     detail_filter=detail_filter,
                     task_type=task_type,
                     status=status,
                     requested_by=requested_by)
            result.append(d)


        # close the cursor
        cur.close()
        return result
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()


def get_jobs():
    """ query data from the vendors table """
    conn = None
    result = list()
    query = """
    SELECT * FROM job
    """
    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query)

        rows = cur.fetchall()

        for row in rows:
            job_id = row[0]
            url = row[1]
            created_at = row[2]
            created_by = row[3]
            start_time = row[4]
            end_time = row[5]
            error_count = row[6]
            result.append(dict(job_id=job_id,
                               url=url,
                               created_at=created_at,
                               created_by=created_by,
                               start_time=start_time,
                               end_time=end_time,
                               error_count=error_count))


        # close the cursor
        cur.close()
        return result
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def get_detail(id):
    """get detail of data by id"""
    conn = None
    query = """
    SELECT * FROM job WHERE job_id = %s
    """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (id,))

        details = cur.fetchall()

        for d in details:
            job_id = d[0]
            url = d[1]
            created_at = d[2]
            created_by = d[3]
            start_time = d[4]
            end_time = d[5]
            error_count = d[6]
            dic = dict(
                job_id=job_id,
                url=url,
                created_at=created_at,
                created_by=created_by,
                start_time=start_time if start_time is not None else "-",
                end_time=end_time if end_time is not None else "-",
                error_count=error_count if error_count is not None else "0"
            )


        cur.close()
        return dic
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        if conn is not None:
            conn.close()

def delete_task(id):

    conn = None
    query = """
    DELETE FROM task WHERE task_id = %s
    """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()

        cur.execute(query, (id,))

        conn.commit()
        print("id %s was deleted" % id)
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("Oops: ", e)
    finally:
        if conn is not None:
            conn.close()

def delete_job(id):

    conn = None
    query = """
    DELETE FROM job WHERE job_id = %s
    """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()

        cur.execute(query, (id,))

        conn.commit()
        print("id %s was deleted" % id)
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("Oops: ", e)
    finally:
        if conn is not None:
            conn.close()

def delete_user(user_id):

    conn = None
    query = """
    DELETE FROM users WHERE user_id = %s
    """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()

        cur.execute(query, (user_id,))

        conn.commit()
        print("User with id %s was deleted" % user_id)
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("Oops: ", e)
    finally:
        if conn is not None:
            conn.close()

def get_users():
    """ query data from the vendors table """
    conn = None
    result = list()
    query = """
    SELECT user_id, username, password FROM users
    """
    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query)

        rows = cur.fetchall()

        for row in rows:
            user_id = row[0]
            username = row[1]
            password = row[2]
            d = dict(user_id=user_id, username=username, password=password)
            result.append(d)

        # close the cursor
        cur.close()
        return result
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def check_username(username, password):
    """ query data from the vendors table """
    conn = None
    result = None
    query = """
    SELECT password FROM users WHERE username = %s
    """
    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (username,))

        result = cur.fetchone()[0]

        # close the cursor
        cur.close()
        if result == password:
            return "success"
        else:
            return "error"
    except (Exception, pg.DatabaseError) as e:
        return "error"
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def dropTable(tableName):

    conn = None
    query = "DROP TABLE IF EXISTS {}".format(tableName)
    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query)

        cur.close()
        conn.commit()
        print("%s table was deleted" % tableName)

    except (Exception, pg.DatabaseError) as e:
        print("Oops: ", e)
    finally:
        if conn is not None:
            conn.close()


if __name__ == "__main__":
    # connect()
    create_tables()
    # task_name = "Test"
    # url = "https://www.perkakasku.com/mesin-blower-baterai-tool-only-bosch-gbl-18-v-li-pt086.html"
    # insert_task(task_name=task_name, index_url=url, task_type="brand_list")
    # print(get_tasks())
    # add = insert_user(username="farhan", password="daruma123")
    # print(add)
    # print(get_users())
    # print(check_username("faan", "daruma123"))

    # dropTable("item")
    # print(json.dumps(get_tasks(), indent=4))
    # print(json.dumps(get_detail(2), indent=4))
    # keys = [x for x in get_detail(1).keys()]
    # print(keys)
    # delete_task(5)
    # print(get_tasks())